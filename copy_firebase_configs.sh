#!/usr/bin/env bash

if [ "$1" == "prod" ];
then
  echo "Switching to Firebase Production environment"
  yes | cp -rf "firebase_environments/prod/google-services.json" android/app
  yes | cp -rf "firebase_environments/prod/GoogleService-Info.plist" ios/ProInYourPocketCustomer
else
  echo "Switching to Firebase Dev environment"
  yes | cp -rf "firebase_environments/dev/google-services.json" android/app
  yes | cp -rf "firebase_environments/dev/GoogleService-Info.plist" ios/ProInYourPocketCustomer
fi