import firebase from "react-native-firebase";
import { resizeImageForUpload } from "../util/image";
import { getCurrentState } from "../redux";

/**
 * Resizes the provide profile photo and uploads it to Firebase Storage.
 * @param uri
 * @returns {Promise<FileReference>}
 */
export async function uploadProfilePhoto(uri) {
  const state = getCurrentState();
  const userId = state.auth.user.uid;
  if (!userId) {
    throw new Error("User is not authenticated.");
  }

  //const resizedUri = await resizeImageForUpload(uri);
  const fileRef = firebase.storage().ref(`/images/${userId}/profilePhoto.jpg`);
  // await fileRef.putFile(resizedUri, {
  //   contentType: "image/jpg"
  // });
  await fileRef.putFile(uri, {
    contentType: "image/jpg"
  });
  return fileRef;
}
