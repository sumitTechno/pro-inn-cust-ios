import { store } from "../redux/store";

export function attachOwner(doc) {
  const state = store.getState();
  const userId = state.auth.user.uid;
  return {
    ...doc,
    owner: userId
  };
}
