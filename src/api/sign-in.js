import firebase from "react-native-firebase";

export async function sendSignInLink (email) {
  if (!email) {
    throw new Error('No email was provided:', email)
  }
  const actionCodeSettings = {
    url: "https://proinyourpocket.com",
    handleCodeInApp: true, // must always be true for sendSignInLinkToEmail
    iOS: {
      bundleId: "com.proinyourpocket.customer"
    },
    android: {
      packageName: "com.proinyourpocket.customer",
      installApp: true
      // minimumVersion: '12',
    }
  };

  await firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings);
}