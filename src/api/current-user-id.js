import { store } from "../redux/store";

export function currentUserId() {
  const state = store.getState();
  return state.auth.user.uid;
}
