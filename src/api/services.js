import services from "../../assets/files/services";

export const getServiceById = id => services.find(service => service.id === id);
