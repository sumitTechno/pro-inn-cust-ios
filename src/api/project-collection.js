import firebase from "react-native-firebase";
import { attachOwner } from "./attach-owner";
import { currentUserId } from "./current-user-id";
import { AsyncStorage } from "react-native";

class ProjectCollection {
  constructor() {
    this.projectsRef = firebase.firestore().collection("projects");
  }

  /**
   * Creates a new project with the given parameters.
   * @param owner
   * @param serviceId
   * @param postalCode
   * @param description
   * @param Pimage
   * @param projectVerified
   * @returns {Promise<DocumentReference>}
   */
  async createProject({ serviceId, postalCode, description, Pimage, projectVerified }) {
    const createdAt = new Date().toISOString();
    const projectStatus = "InProgress";
    return await this.projectsRef.add(
      {
        serviceId,
        postalCode,
        description,
        Pimage,
        projectStatus,
        createdAt,
        projectVerified
      }
    ).then(docRef => {
      AsyncStorage.setItem('currentProjectId', docRef.id).then(() => console.log(docRef.id));
    })
  }
  async verifyProject() {
    AsyncStorage.getItem('currentProjectId').then(val => {
      return this.projectsRef.doc(val).set(attachOwner({ projectVerified: 'Y' }), { merge: true }).then(() => console.log('hello'))
    })
  }
  async addOwnerToProject(uid) {
    AsyncStorage.getItem('currentProjectId').then(val => {
      return this.projectsRef.doc(val).set({ projectVerified: 'N', owner: uid }, { merge: true }).then(() => console.log('hello'))
    })
  }
  /**
   * Create a query to list the current user's projects.
   * @returns {Query}
   */
  listMyProjects() {
    return this.projectsRef
      .where("owner", "==", currentUserId())
      .orderBy("createdAt", "desc")
    //.get().then((querySnap) => {
    //   querySnap.forEach(doc => {
    //     doc.ref.set({ projectVerified: 'Y' }, { merge: true })
    //   })
    // })
  }
}

const projects = new ProjectCollection();

export default projects;
