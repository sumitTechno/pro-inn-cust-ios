import services from "../../assets/files/services";

const textStartsWith = (text, query) => {
  return text.toLowerCase().startsWith(query);
};

const tagContainsQuery = query => tag => textStartsWith(tag, query);

const serviceContainsQuery = query => service => {
  const words = query.split(" ").filter(text => !!text);

  let includeService = false;
  for (const word of words) {
    // const nameContainsQuery = textStartsWith(service.name, word);
    const tagsContainQuery =
      service.searchTags.filter(tagContainsQuery(word)).length > 0;
    // if (nameContainsQuery || tagsContainQuery) {
    if (tagsContainQuery) {
      includeService = true;
    }
  }
  return includeService;
};

export function filterServices(query) {
  const lowerCaseQuery = query.toLowerCase();
  return services.filter(serviceContainsQuery(lowerCaseQuery));
}
