/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { AsyncStorage, Alert ,PushNotificationIOS} from "react-native";
import { connect } from "react-redux";
import firebase from "react-native-firebase";


import SplashScreen from "react-native-splash-screen";
import codePush from "react-native-code-push";

import RootNavigator from "./navigators/root-navigator";
import { initializeAuth, setAuthLoading } from "./redux/reducers/auth.reducer";
import NavigationService from "./util/navigation-service";
import DeepLinkGate from "./components/deep-link-gate";
import  { Notification, NotificationOpen } from 'react-native-firebase';
import MyStatusBar from '../src/components/cross-platform-statusbar';


import {
  setAppInitialized,
  setAppInitialLaunch
} from "./redux/reducers/app.reducer";
import { Main } from "../index";
import DeviceInfo from 'react-native-device-info';

import { StackNavigator, NavigationActions, StackActions, createStackNavigator } from 'react-navigation';


const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.ON_NEXT_RESUME
};

const notification = 1;
const db = firebase.firestore();

type Props = {};
class App extends Component<Props> {
  codePushStatusDidChange(status) {
    console.log("[CodePush][App] Sync status changed:", status);
  }

  codePushDownloadDidProgress(progress) {
    console.log("[CodePush][App] Download progress changed:", progress);
  }


  async componentDidUpdate(prevProps) {

    this._messageListener();
    this._notificationOpenedListener();
    //this._notificationForeGroundListner();
   
    
  }

  componentWillMount()
  {
    PushNotificationIOS.requestPermissions();
  }

  async componentDidMount() {
    this._notificationForeGroundListner();
    //this.firebaseAddToken();
    await this.checkPermission();
    await this.initializApp();
    SplashScreen.hide();
   
   
  }


  async _messageListener() {
    firebase.messaging().onMessage((message) => {
      console.log('notification massage : ' + JSON.stringify(message));
    });
  }

  async _notificationOpenedListener() {
    const { user } = this.props;
    firebase.notifications().onNotificationOpened((notificationOpen) => {
      if (user) {
        if (notificationOpen.notification.data.navigate == 'Notifications') {
          NavigationService.navigate("Notifications");
        }
        else {
            NavigationService.navigate(notificationOpen.notification.data.navigate,notificationOpen.notification.data);

          // this.props.navigation.navigate(title.data.navigate,title.data,{})
        }
      }
    }
    );
  }


  _notificationForeGroundListner() {
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      this.showLocalNotification(notification);
    });
  }


  async showLocalNotification(notification1) {
    const { user } = this.props;
    if (user != null) {
      const notification = new firebase.notifications.Notification()
        .setNotificationId('notificationId')
        .setTitle(notification1.title)
        .setBody(notification1.body)
        .setSound('default')
        .setData(notification1.data);
      firebase.notifications().displayNotification(notification)
    }
  }





  

  async checkPermission()
  {
    firebase.messaging().hasPermission()
      .then(enabled => {
        if (enabled) {
          // user has permissions
          console.log('User has fcm permission')
          this.firebaseAddToken();
        } else {
          // user doesn't have permission
          console.log('User doesn\'t have fcm permission')
          firebase.messaging().requestPermission()
            .then(() => {
              // User has authorised
              console.log('User has authorised fcm')
              this.firebaseAddToken();
            })
            .catch(error => {
              // User has rejected permissions
              console.log('User has rejected fcm permissions, error = ', error)
            })
        }
      })
  }

  async firebaseAddToken()
  {
    const { user } = this.props;
    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          //alert(fcmToken);
          AsyncStorage.setItem('fcm_token', fcmToken);
          console.log("fcm token" + fcmToken);
          this._addFcm(fcmToken);
        }
        else {
            //alert("no token");
             //this.initializApp();
          // user doesn't have a device token yet
        }
      });
  }

  _addFcm(fcmtoken)
  {
    const { user } = this.props;
    console.log("get fcm"+fcmtoken);
    if (user != null) {
      const deviceId = DeviceInfo.getUniqueID();
      const deviceModel = DeviceInfo.getModel();

      console.log(user);
      console.log("device id" + deviceId);
      console.log("get fcm"+fcmtoken);
      db.collection("tokens").doc(fcmtoken+"_"+user.uid).set({
        uid: user.uid,
        name: user.displayName,
        fcm_token:fcmtoken,
        device_id: deviceId,
        device_Model: deviceModel,
        createTime: new Date()

      }).then((user) => {
        console.log("successfully add fcm ");
        this._addUser();
        //this.initializApp();
        
      }).catch((error) => {
        console.log("user error" + error)
      })
    }
    else
    {
      //this.initializApp();
    }
  }


  _addUser() {
    //this.setState({ isLoading: true });
    const { user } = this.props;
    var name="";
    if( user.displayName == undefined ||  user.displayName == null)
    {
      name="Customer";
    }
    else
    {
      name=user.displayName;
    }
    db.collection("users").doc(user.uid).set({
      firstname: name,
      lastname: "",
      email: user.email,
      phone: user.phoneNumber,
      imageLink: user.photoURL,
      //password:this.state.password,
      uid: user.uid,
      createdAt: user.creationTime,
      phVerify_status: 'N'
    }).then((user) => {
      //this.setState({ isLoading: false });
    }).catch((error) => {
      this.setState({ isLoading: false });
      console.log("user error" + error)
    })
  }

  async initializApp() {

    firebase.analytics().setAnalyticsCollectionEnabled(true);
    this.props.initializeAuth();
    await this.checkFirstLaunch();
    this.continueIfNeeded();
    this.props.setAppInitialized();
  }

  checkFirstLaunch = async () => {
    const appLoaded = await AsyncStorage.getItem("app-loaded");

    // First time load
    if (!appLoaded || appLoaded !== "true") {
      await this.handleInitialLaunch();
    }
  };

  handleInitialLaunch = async () => {
    this.props.setAppInitialLaunch();
    // Clear any previous auth states. Squelch errors.
    try {
      await firebase.auth().signOut();
    } catch (err) { }
    // Show onboarding screens.
    if (notification == 1) {
      NavigationService.navigate("Onboarding");
      await AsyncStorage.setItem("app-loaded", "true");
    }
    else {
      console.log("notification" + notification);
      NavigationService.navigate("NewProject");
      //await AsyncStorage.setItem("app-loaded", "true");
    }

  };

  continueIfNeeded = () => {
    if (this.props.newProject.postalCode && this.props.newProject.description) {
      NavigationService.navigate("NewProject");
    }
  };



  render() {
    return (
      <DeepLinkGate>
        <MyStatusBar backgroundColor={'#315CAB'} />
        <RootNavigator
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </DeepLinkGate>
    );
  }
}

App = codePush(codePushOptions)(App);



const mapStateToProps = ({ auth, newProject, app }) => ({
  email: auth.email,
  user: auth.user,
  newProject,
  initialized: app.initialized
});

export default connect(
  mapStateToProps,
  { initializeAuth, setAuthLoading, setAppInitialized, setAppInitialLaunch }
)(App);
