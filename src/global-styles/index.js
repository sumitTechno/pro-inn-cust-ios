import { StyleSheet } from "react-native";
import theme from "../theme";
export default StyleSheet.create({
  flex1: {
    flex: 1
  },
  defaultContainer: {
    backgroundColor: theme.colors.background,
    flex: 1
  },
  surfaceContainer: {
    backgroundColor: theme.colors.surface,
    flex: 1
  },
});
