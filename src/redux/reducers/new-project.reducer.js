const action_set_postal_code = "[new-project] SET_POSTAL_CODE";
const action_set_description = "[new-project] SET_DESCRIPTION";
const action_set_project_verified = "[new-project] SET_PROJECT_VERIFIED"
const action_set_service_id = "[new-project] SET_SERVICE";
const action_set_project_image = "[new-project] SET_IMAGE";
const action_reset = "[new-project] RESET";

const initialState = {
  serviceId: "",
  postalCode: "",
  description: "",
  Pimage: [],
  isVerified: false,
  projectVerified: 'N'
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case action_set_postal_code:
    case action_set_description:
    case action_set_project_image:
    case action_set_service_id: {
      return {
        ...state,
        ...action.payload
      };
    }
    case action_set_project_verified: {
      return {
        ...state,
        ...action.payload
      }
    }

    case action_reset: {
      return {
        ...initialState
      };
    }
    default:
      return state;
  }
}

export function setServiceId(serviceId) {
  return {
    type: action_set_service_id,
    payload: { serviceId }
  };
}

export function setPostalCode(postalCode) {
  return {
    type: action_set_postal_code,
    payload: { postalCode }
  };
}

export function setUserVerified(isVerified) {
  return {
    type: action_set_user_status,
    payload: { isVerified }
  };
}

export function setDescription(description) {
  return {
    type: action_set_description,
    payload: { description }
  };
}

export function setProjectImage(Pimage) {
  return {
    type: action_set_project_image,
    payload: { Pimage }
  };
}


export function resetProject() {
  return {
    type: action_reset
  };
}
export function setProjectVerified(projectVerified) {
  return {
    type: action_set_project_verified,
    payload: { projectVerified }
  }
}

export default reducer;
