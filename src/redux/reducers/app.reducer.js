const action_set_initialized = "[new-project] SET_INITIALIZED";
const action_set_initial_launch = "[new-project] SET_INITIAL_LAUNCH";

const initialState = {
  initialized: false,
  initialLaunch: false
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case action_set_initial_launch: {
      return {
        ...state,
        initialLaunch: true
      };
    }
    case action_set_initialized: {
      return {
        ...state,
        initialized: true
      };
    }
    default:
      return state;
  }
}

// This should be called BEFORE setAppInitialized.
export const setAppInitialLaunch = () => ({
  type: action_set_initial_launch
});

export const setAppInitialized = () => ({
  type: action_set_initialized
});

export default reducer;
