import { filterServices } from "../../api/search-data-source";

const action_update_query = "[search] UPDATE_QUERY";

const initialState = {
  query: "",
  results: []
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case action_update_query: {
      const { query } = action.payload;
      const results = filterServices(query);
      return {
        ...state,
        ...action.payload,
        results
      };
    }
    default:
      return state;
  }
}

export function updateSearchQuery(query) {
  return {
    type: action_update_query,
    payload: {
      query
    }
  };
}

export default reducer;
