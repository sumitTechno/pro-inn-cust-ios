import { combineReducers } from "redux";
import auth from "./auth.reducer";
import newProject from "./new-project.reducer";
import search from "./search.reducer";
import app from "./app.reducer";


export default combineReducers({
  auth,
  newProject,
  search,
  app
});
