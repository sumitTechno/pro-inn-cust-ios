import firebase, { firestore } from "react-native-firebase";
const db = firebase.firestore();

const action_sign_in = "[auth] SIGN_IN";
const action_force_sign_in = "[aurh] FORCE_SIGN_IN"
const action_sign_out = "[auth] SIGN_OUT";
const action_set_auth_email = "[auth] SET_AUTH_EMAIL";
const action_set_auth_name = "[auth] SET_AUTH_NAME";
const action_set_auth_loading = "[auth] SET_AUTH_LOADING";

const initialState = {
  user: null,
  email: null,
  name: null,
  initialized: false,
  loading: false,
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case action_force_sign_in:
      return {
        ...state,
        ...action.payload
      };
    case action_sign_in: {
      return {
        ...state,
        initialized: true,
        ...action.payload
      };
    }
    case action_sign_out: {
      return {
        ...state,
        user: null,
        initialized: true
      };
    }
    case action_set_auth_email:
    case action_set_auth_name:
    case action_set_auth_loading: {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state;
  }
}

export const initializeAuth = () => dispatch => {
  firebase.auth().onUserChanged(user => {
    if (user) {
      firebase.analytics().setUserId(user.uid);
      db.collection('users').doc(user.uid).set({ email_verify_status: 'Y' }, { merge: true })
      dispatch(signIn(user));
    } else {
      dispatch(signOut());
    }
  });
  return null;
};

export function signIn(user) {
  return {
    type: action_sign_in,
    payload: {
      user
    }
  };
}

export function signOut() {
  return {
    type: action_sign_out
  };
}

export function setAuthEmail(email) {
  return {
    type: action_set_auth_email,
    payload: {
      email
    }
  };
}

export function setAuthName(name) {
  return {
    type: action_set_auth_name,
    payload: {
      name
    }
  };
}

export function setAuthLoading(loading) {
  return {
    type: action_set_auth_loading,
    payload: {
      loading
    }
  };
}

export default reducer;
