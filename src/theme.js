import { DefaultTheme } from "react-native-paper";

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#315cab',
    accent: '#F19C00',
    textSecondary: 'rgba(0, 0, 0, .60)'
  }
};

export default theme;
