import React from "react";
import PropTypes from "prop-types";
import { View, StyleSheet, ActivityIndicator } from "react-native";
import { Appbar, TouchableRipple } from "react-native-paper";
import theme from "../theme";
import HeaderAction from "./header-action";

const AsyncHeaderAction = ({ loading, icon, ...rest }) => {
  if (loading) {
    return (
      <View style={styles.loadingIndicatorContainer}>
        <ActivityIndicator
          animating={true}
          color={theme.colors.primary}
          size="small"
        />
      </View>
    );
  }
  return <HeaderAction icon={icon} {...rest} />;
};

const styles = StyleSheet.create({
  loadingIndicatorContainer: TouchableRipple.supported
    ? {
        height: 28,
        width: 28,
        margin: 10,
        alignItems: "center",
        justifyContent: "center"
      }
    : {
        borderRadius: 36 / 2,
        height: 36,
        width: 36,
        margin: 6,
        alignItems: "center",
        justifyContent: "center"
      }
});

AsyncHeaderAction.propTypes = {
  icon: PropTypes.string.isRequired,
  loading: PropTypes.bool
};

AsyncHeaderAction.defaultProps = {
  loading: false
};

export default AsyncHeaderAction;
