import React, { PureComponent } from "react";
import { FlatList, StyleSheet, Linking } from "react-native";
import PropTypes from "prop-types";
import { List, Divider, Text } from "react-native-paper";
import theme from "../theme";

const RequestService = () => (
  <Text
    style={styles.requestText}
    onPress={() => Linking.openURL("https://goo.gl/forms/JYINqwIn8Buv2TM92")}
  >
    Not seeing the service you are looking for?
  </Text>
);

class SearchResultList extends PureComponent {
  renderItem = ({ item }) => (
    <List.Item
      title={item.name}
      onPress={() => this.props.serviceSelected(item.id)}
    />
  );

  keyExtractor = item => item.id;

  render() {
    return (
      <FlatList
        data={this.props.results}
        renderItem={this.renderItem}
        keyExtractor={this.keyExtractor}
        ListFooterComponent={() => <RequestService />}
        ItemSeparatorComponent={() => <Divider />}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps="always"
      />
    );
  }
}

const styles = StyleSheet.create({
  requestText: {
    padding: 16,
    color: theme.colors.primary
  }
});

SearchResultList.propTypes = {
  results: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string
    })
  ),
  serviceSelected: PropTypes.func
};

export default SearchResultList;
