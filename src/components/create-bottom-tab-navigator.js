import * as React from "react";
import { Platform, Keyboard } from "react-native";
import { BottomNavigation } from "react-native-paper";
import { createTabNavigator } from "react-navigation-tabs";

class BottomNavigationView extends React.Component {
  state = {
    visible: true
  };

  componentDidMount() {
    if (Platform.OS === "android") {
      this.keyboardEventListeners = [
        Keyboard.addListener("keyboardDidShow", this.setVisibility(false)),
        Keyboard.addListener("keyboardDidHide", this.setVisibility(true))
      ];
    }
  }

  componentWillUnmount() {
    this.keyboardEventListeners &&
      this.keyboardEventListeners.forEach(eventListener =>
        eventListener.remove()
      );
  }

  setVisibility = visible => () => this.setState({ visible });

  _getColor = ({ route }) => {
    const { descriptors } = this.props;
    const descriptor = descriptors[route.key];
    const options = descriptor.options;

    return options.tabBarColor;
  };

  _isVisible() {
    const { navigation, descriptors } = this.props;
    const { state } = navigation;
    const route = state.routes[state.index];
    const options = descriptors[route.key].options;
    return options.tabBarVisible;
  }

  _renderIcon = ({ route, focused, color }) => {
    return this.props.renderIcon({ route, focused, tintColor: color });
  };

  render() {
    const {
      activeTintColor,
      inactiveTintColor,
      navigation,
      // eslint-disable-next-line no-unused-vars
      descriptors,
      barStyle,
      ...rest
    } = this.props;

    const isVisible = this.state.visible && this._isVisible();
    const extraStyle =
      typeof isVisible === "boolean"
        ? { display: isVisible ? null : "none" }
        : null;

    return (
      <BottomNavigation
        // Pass these for backward compaibility
        activeColor={activeTintColor}
        inactiveColor={inactiveTintColor}
        {...rest}
        renderIcon={this._renderIcon}
        barStyle={[barStyle, extraStyle]}
        navigationState={navigation.state}
        getColor={this._getColor}
        shifting={true}
      />
    );
  }
}

const createBottomTabNavigator = createTabNavigator(BottomNavigationView);

export default createBottomTabNavigator;
