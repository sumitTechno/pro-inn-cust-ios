import React, { Component } from "react";
import PropTypes from "prop-types";
import { Image, Text, View, StyleSheet } from "react-native";
import { Appbar } from "react-native-paper";
import theme from "../theme";
import HeaderAction from "./header-action";
import AsyncHeaderAction from "./async-header-action";
import bannerWhite from "../../assets/images/banner-white.png";
import FastImage from "react-native-fast-image";

const headerTheme = {
  colors: {
    primary: "#ffffff"
  }
};

class NavigationHeader extends Component {
  render() {
    const { title, showBackButton, actions, style } = this.props;
    return (
      <Appbar.Header theme={headerTheme} style={[{ elevation: 0,zIndex:0 }, style]}>
        {showBackButton && (
          <Appbar.BackAction
            onPress={this.goBack}
            color={theme.colors.primary}
          />
        )}
        {title !== null &&
          (typeof title === "string" ? (
            <Appbar.Content title={title} />
          ) : (
            <View style={styles.titleContainer}>{title}</View>
          ))}
        {actions}
      </Appbar.Header>
    );
  }

  goBack = () => {
    const { navigation, onBack } = this.props;
    if (onBack) {
      return onBack();
    }
    navigation.goBack();
  };
}

const styles = StyleSheet.create({
  titleContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

NavigationHeader.propTypes = {
  navigation: PropTypes.object.isRequired,
  title: PropTypes.node,
  showBackButton: PropTypes.bool,
  actions: PropTypes.arrayOf(PropTypes.object),
  onBack: PropTypes.func
};

NavigationHeader.defaultProps = {
  title: "",
  showBackButton: true,
  actions: [],
  onBack: null
};

export default NavigationHeader;
