import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { FlatList, TouchableOpacity, View, Image, Text,StyleSheet } from "react-native";
import ProjectListItem from "./project-list-item";
import { getServiceById } from "../api/services";
import { Divider } from "react-native-paper";

class SelectImageBtn extends PureComponent {

    render() {
        const { onClick } = this.props;

        return (
            <TouchableOpacity style={[styles.clickToUpload, { justifyContent: "center", alignItems: "center" }]}
                onPress={() => onClick()}
            >
                <View style={{ width: 30, height: 30, marginBottom: 10, marginTop: 10 }}>
                    <Image
                        source={require('../../assets/images/up.png')}
                        style={{ width: 30, height: 30 }}
                    />
                </View>
                <Text style={{ color: "#5f5e5e", fontSize: 12 }}>Click To upload image</Text>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    clickToUpload: {
        elevation: 1,
        borderColor: "#d2d1d1",
        justifyContent: "center",
        alignItems: "center",
        padding: 5
    }
})
SelectImageBtn.propTypes = {
    onClick: PropTypes.func
};

export default SelectImageBtn;
