import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";


class ProjectListItemBtn extends PureComponent {
    render() {
        const { onEdit, onDelete, onUndo, project } = this.props;
        const shortenedDescription = `${description.slice(0, 75)}${
            description.length > 75 ? "..." : ""
            }`;
        return (
            (project.projectStatus != 'Completed' && project.projectStatus != 'Deleted') ? (
                <View style={{ flexDirection: 'row', justifyContent: "space-around" }}>
                    <TouchableOpacity style={{ padding: 10, marginHorizontal: 10, }} onPress={onEdit}>
                        <Text style={{ color: 'blue', textAlign: 'center' }}>Edit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ padding: 10, marginHorizontal: 10, }} onPress={onDelete}>
                        <Text style={{ color: 'red', textAlign: 'center' }}>Delete</Text>
                    </TouchableOpacity>
                </View>
            ) : (project.projectStatus == 'Deleted') ? (
                <View style={{ flexDirection: 'row', justifyContent: "space-around" }}>
                    <TouchableOpacity style={{ padding: 10, marginHorizontal: 10, alignSelf: 'flex-end' }} onPress={onUndo}>
                        <Text style={{ color: 'red', textAlign: 'center' }}>Undo</Text>
                    </TouchableOpacity>
                </View>
            ) : null
        );
    }
}

const styles = StyleSheet.create({});

ProjectListItemBtn.propTypes = {
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
    onUndo: PropTypes.func,
    project: PropTypes.object(
        PropTypes.shape({
            id: PropTypes.string,
            description: PropTypes.string
        })
    ),
};

export default ProjectListItem;
