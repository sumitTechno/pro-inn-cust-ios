import React, { Component } from "react";
import PropTypes from "prop-types";
import { FlatList, StyleSheet } from "react-native";
import FeaturedServiceListItem from "./featured-service-list-item";

class FeaturedServiceList extends Component {
  onServiceSelected = id => console.log("Selected service", id);

  renderItem = ({ item }) => (
    <FeaturedServiceListItem
      service={item}
      serviceSelected={this.props.serviceSelected}
    />
  );

  keyExtractor = item => item.id;

  render() {
    const { services } = this.props;
    return (
      <FlatList
        contentContainerStyle={styles.list}
        renderItem={this.renderItem}
        keyExtractor={this.keyExtractor}
        data={services}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />
    );
  }
}

const styles = StyleSheet.create({
  list: {
    paddingHorizontal: 16,
    paddingTop: 0,
    paddingBottom: 16,
  }
});

FeaturedServiceList.propTypes = {
  services: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string
    })
  ),
  serviceSelected: PropTypes.func.isRequired
};

export default FeaturedServiceList;
