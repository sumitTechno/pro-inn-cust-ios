import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import PropTypes from "prop-types";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { TextInput } from "react-native-paper";

import theme from "../theme";

class IconTextInput extends Component {
  state = {
    focused: false
  };

  render() {
    const { icon, style, inputRef, color, ...rest } = this.props;
    return (
      <View style={[styles.row, style]}>
        <MaterialIcons
          name={icon}
          size={24}
          style={styles.icon}
          color={this.iconColor()}
        />
        <TextInput
          theme={{
            colors: {
              primary: this.iconColor(),
              background: theme.colors.surface
            }
          }}
          mode="outlined"
          style={styles.input}
          onFocus={() => this.setState({ focused: true })}
          onBlur={() => this.setState({ focused: false })}
          ref={inputRef}
          {...rest}
        />
      </View>
    );
  }

  iconColor = () =>
    this.state.focused
      ? this.props.color || theme.colors.primary
      : theme.colors.disabled;
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "row"
  },
  icon: {
    marginRight: 16,
    paddingTop: 24
  },
  input: {
    flex: 1
  }
});

IconTextInput.propTypes = {
  icon: PropTypes.string,
  inputRef: PropTypes.func,
  color: PropTypes.string
};

export default IconTextInput;
