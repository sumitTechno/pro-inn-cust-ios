import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { Caption, List } from "react-native-paper";
import Moment from "react-moment";

class ProjectListItem extends PureComponent {
  render() {
    const { title, description, createdAt, onEdit, onDelete,onUndo, project } = this.props;
    const shortenedDescription = `${description.slice(0, 75)}${
      description.length > 75 ? "..." : ""
      }`;
    return (
      <List.Item
        title={title}
        description={shortenedDescription}
        // left={props => <List.Icon {...props} icon="info" />}
        right={props => (
          <View style={{ alignItems: 'flex-end', }}>
            <Moment element={Caption} fromNow style={{ maxWidth: 96, }}>
              {createdAt}
            </Moment>
            {(project.projectStatus != 'Completed' && project.projectStatus != 'Deleted') ? (
              <View style={{ flexDirection: 'row', justifyContent: "space-around" }}>
                <TouchableOpacity style={{ padding: 10, marginHorizontal: 10, }} onPress={onEdit}>
                  <Text style={{ color: 'blue', textAlign: 'center' }}>Edit</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ padding: 10, marginHorizontal: 10, }} onPress={onDelete}>
                  <Text style={{ color: 'red', textAlign: 'center' }}>Delete</Text>
                </TouchableOpacity>
              </View>
            ) : null}
            {(project.projectStatus == 'Deleted') ? (
              <View style={{ flexDirection: 'row', justifyContent: "space-around" }}>
                <TouchableOpacity style={{ padding: 10, marginHorizontal: 10,alignSelf:'flex-end' }} onPress={onUndo}>
                  <Text style={{ color: 'red', textAlign: 'center' }}>Move Back To Progress</Text>
                </TouchableOpacity>
              </View>
            ) : null}
          </View>

        )}
      />
    );
  }
}

const styles = StyleSheet.create({});

ProjectListItem.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default ProjectListItem;
