import React from "react";
import { SafeAreaView, ScrollView, StyleSheet, Text } from "react-native";
import { Button, Title, Card, Subheading } from "react-native-paper";
import theme from "../theme";
import IconTextInput from "./icon-text-input";
import { withNavigation } from "react-navigation";
import profileCard from "../../assets/images/profile-card.png";

const LoginPlaceholder = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <Card>
          <Card.Cover source={profileCard} />
          <Card.Content>
            <Title style={[styles.condensed, styles.title]}>Sign in</Title>
            <Subheading>
              Sign in to view your account. If you do not have an account, you
              can also create one!
            </Subheading>
            <Button
              style={styles.button}
              mode="outlined"
              onPress={() => navigation.navigate("SignIn")}
            >
              Continue
            </Button>
          </Card.Content>
        </Card>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = {
  container: {
    backgroundColor: theme.colors.background,
    flex: 1
  },
  contentContainer: {
    padding: 16,
    alignItems: "center"
  },
  title: {
    alignSelf: "center"
  },
  button: {
    alignSelf: "center",
    marginTop: 16
  }
};

export default withNavigation(LoginPlaceholder);
