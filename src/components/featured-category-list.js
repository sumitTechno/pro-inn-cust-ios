import React, { Component } from "react";
import { FlatList, StyleSheet, View } from "react-native";
import PropTypes from "prop-types";
import FeaturedServiceList from "./featured-service-list";
import { Title } from "react-native-paper";

class FeaturedCategoryList extends Component {
  renderItem = ({ item }) => (
    <View>
      <Title style={styles.categoryTitle}>{item.name}</Title>
      <FeaturedServiceList
        services={item.services}
        serviceSelected={this.props.serviceSelected}
      />
    </View>
  );

  keyExtractor = item => item.id;

  render() {
    const { featuredCategories, serviceSelected, ...rest } = this.props;
    return (
      <FlatList
        renderItem={this.renderItem}
        keyExtractor={this.keyExtractor}
        horizontal={false}
        data={featuredCategories}
        {...rest}
      />
    );
  }
}

const styles = StyleSheet.create({
  categoryTitle: {
    marginLeft: 24
  }
});

FeaturedCategoryList.propTypes = {
  featuredCategories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      services: PropTypes.array
    })
  ),
  serviceSelected: PropTypes.func.isRequired
};

export default FeaturedCategoryList;
