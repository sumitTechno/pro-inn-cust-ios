import React from "react";
import PropTypes from "prop-types";
import { Appbar } from "react-native-paper";
import theme from "../theme";

const HeaderAction = ({ icon, onPress, disabled }) => {
  const iconColor = disabled ? theme.colors.disabled : theme.colors.primary;
  return (
    <Appbar.Action
      icon={icon}
      color={iconColor}
      onPress={onPress}
      disabled={disabled}
    />
  );
};

HeaderAction.propTypes = {
  icon: PropTypes.string.isRequired,
  onPress: PropTypes.func
};

export default HeaderAction;
