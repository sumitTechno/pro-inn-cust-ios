
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity, FlatList, TextInput, Modal,StatusBar } from "react-native";

import DeviceInfo from 'react-native-device-info'

export default class MyStatusBar extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { backgroundColor } = this.props;
        const hasNotch = DeviceInfo.hasNotch();
        const model = DeviceInfo.getModel();
        return (
            <View style={[{ height: hasNotch ? (model == 'iPhone XR' ? 40 : 37) : 20 }, { backgroundColor }]}>
                <StatusBar translucent backgroundColor={backgroundColor} />
            </View>
        );
    }
}


MyStatusBar.propTypes = {
    backgroundColor: PropTypes.string,
}
