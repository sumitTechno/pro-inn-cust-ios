import React, { PureComponent } from "react";
import { View, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import { Subheading, Card } from "react-native-paper";
import FastImage from "react-native-fast-image";

class FeaturedServiceListItem extends PureComponent {
  onPress = () => {
    console.log("Service selected.");
  };

  render() {
    const { service, serviceSelected } = this.props;
    return (
      <Card style={styles.card} onPress={() => serviceSelected(service.id)}>
        <View style={styles.cardCoverContainer}>
          <FastImage
            style={styles.cardCover}
            source={{ uri: service.firebaseDownloadUrl }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </View>
        <Card.Content style={styles.cardContent}>
          <Subheading numberOfLines={2} ellipsizeMode="tail">
            {service.name}
          </Subheading>
        </Card.Content>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    marginHorizontal: 8,
    marginBottom: 16,
    marginTop: 8,
    maxWidth: 200,
    width: 200
  },
  cardContent: {
    paddingHorizontal: 16,
    paddingVertical: 4
  },
  cardCoverContainer: {
    backgroundColor: "#eeeeee",
    overflow: "hidden",
    width: 200,
    height: 113,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4
  },
  cardCover: {
    flex: 1,
    padding: 16,
    justifyContent: "flex-end",
    width: 200,
    height: 113
  }
});

FeaturedServiceListItem.propTypes = {
  service: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    firebaseDownloadUrl: PropTypes.string
  }),
  serviceSelected: PropTypes.func
};

export default FeaturedServiceListItem;
