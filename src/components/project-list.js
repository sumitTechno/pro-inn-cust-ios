import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { FlatList, TouchableOpacity } from "react-native";
import ProjectListItem from "./project-list-item";
import { getServiceById } from "../api/services";
import { Divider } from "react-native-paper";

class ProjectList extends PureComponent {
  renderItem = ({ item }) => {
    const service = getServiceById(item.serviceId);
    if (!service) {
      console.log("SERVICE NOT FOUND:", item.serviceId, item);
      return null;
    }
    return (
      <TouchableOpacity onPress={() => this.props.onProjectSelect(item)}>
        <ProjectListItem
          title={service.name}
          description={item.description}
          createdAt={item.createdAt}
          project={item}
          onEdit={() => this.props.onEditProject(item)}
          onDelete={() => this.props.onDeleteProject(item)}
          onUndo={()=>this.props.onUndoDeletedProject(item)}
        />
      </TouchableOpacity>
    );
  };

  keyExtractor = item => item.id;

  render() {
    const { projects } = this.props;
    return (
      <FlatList
        renderItem={this.renderItem}
        keyExtractor={this.keyExtractor}
        ItemSeparatorComponent={() => <Divider />}
        data={projects}
      />
    );
  }
}

ProjectList.propTypes = {
  projects: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      description: PropTypes.string
    })
  ),
  onProjectSelect: PropTypes.func,
  onEditProject:PropTypes.func,
  onDeleteProject:PropTypes.func,
  onUndoDeletedProject:PropTypes.func
};

export default ProjectList;
