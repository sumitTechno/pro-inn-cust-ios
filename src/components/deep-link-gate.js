import React, { Component } from "react";
import PropTypes from "prop-types";
import firebase from "react-native-firebase";
import { connect } from "react-redux";
import { setAuthLoading } from "../redux/reducers/auth.reducer";
import { Linking } from "react-native"

class DeepLinkGate extends Component {
  componentDidMount() {
    this.checkInitialLink();
    this.registerLinkListener();
  }

  checkInitialLink = () => {
    Linking.getInitialURL().then(async url => {//if causes crash remove getInitial
      if (url) {
        if (firebase.auth().isSignInWithEmailLink(url)) {
          await this.handleSignInLink(url);
        }
      }
    })
  };

  registerLinkListener = () => {
    Linking.addEventListener('url', this.handleLink);
    // firebase.links().onLink(url => this.handleLink(url, false));
  };


  handleLink = async (ev) => {
    if (firebase.auth().isSignInWithEmailLink(ev.url)) {
      await this.handleSignInLink(ev.url);
    }
  };

  handleSignInLink = async link => {
    if (!this.props.email) {
      return;
    }

    try {
      this.props.setAuthLoading(true);
      firebase.analytics().logEvent("sign_in");
      await firebase.auth().signInWithEmailLink(this.props.email, link);
      this.props.setAuthLoading(false);
    } catch (err) {
      this.props.setAuthLoading(false);
      console.log("Error signing in with email link:", err);
    }
  };

  render() {
    return this.props.children;
  }
}

DeepLinkGate.propTypes = {
  children: PropTypes.node
};

const mapStateToProps = ({ auth, app }) => ({
  initialLaunch: app.initialLaunch,
  initialized: app.initialized,
  email: auth.email
});
export default connect(
  mapStateToProps,
  { setAuthLoading }
)(DeepLinkGate);
