import React, { Component } from "react";
import {
  ScrollView,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  AsyncStorage
} from "react-native";
import tabBarIcon from "../components/tab-bar-icon";
import { Searchbar } from "react-native-paper";
import FeaturedCategoryList from "../components/featured-category-list";
import services from "../../assets/files/services";
import featuredCategories from "../../assets/files/featured-categories";
import theme from "../theme";
import { connect } from "react-redux";
import {
  resetProject,
  setServiceId
} from "../redux/reducers/new-project.reducer";
import firebase from "react-native-firebase";

const SearchBarButton = ({ onPress, style }) => (
  <SafeAreaView style={style}>
    <TouchableOpacity onPress={onPress}>
      <Searchbar
        pointerEvents="none"
        style={styles.searchBar}
        placeholder="Search for a service"
      />
      <View style={styles.searchBarCover} />
    </TouchableOpacity>
  </SafeAreaView>
);

const getServiceById = id => services.find(service => service.id === id);

class HomeScreen extends Component {
  static navigationOptions = {
    tabBarIcon: tabBarIcon("search"),
    tabBarLabel: "Discover"
  };

  state = {
    featuredCategories: [],
    notif:""
  };

  async componentDidMount() {
    firebase.analytics().setCurrentScreen("Home", "Home");
    //this.checkPermission();
    this.loadFeaturedCategories();

    // alert();
    //   const notify=await AsyncStorage.getItem("notify");
    //   alert(notify);
    //   if(notify=="true")
    //   {
    //     await AsyncStorage.setItem("notify", "false");
    //     this.props.navigation.navigate("NewProject")
    //   }
    //   else
    //   {
    //     firebase.analytics().setCurrentScreen("Home", "Home");
    //     this.loadFeaturedCategories();
    //   }
      
  }


  async checkPermission()
  {
    firebase.messaging().hasPermission()
      .then(enabled => {
        if (enabled) {
          // user has permissions
          console.log('User has fcm permission')
          this.firebaseAddToken();
        } else {
          // user doesn't have permission
          console.log('User doesn\'t have fcm permission')
          firebase.messaging().requestPermission()
            .then(() => {
              // User has authorised
              console.log('User has authorised fcm')
              this.firebaseAddToken();
            })
            .catch(error => {
              // User has rejected permissions
              console.log('User has rejected fcm permissions, error = ', error)
            })
        }
      })
  }

  async firebaseAddToken()
  {
    const { user } = this.props;
    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          //alert(fcmToken);
          AsyncStorage.setItem('fcm_token', fcmToken);
          console.log("fcm token" + fcmToken);
          this._addFcm(fcmToken);
        }
        else {
          this.loadFeaturedCategories();
        }
      });
  }

  _addFcm(fcmtoken)
  {
    const { user } = this.props;
    console.log("get fcm"+fcmtoken);
    if (user != null) {
      const deviceId = DeviceInfo.getUniqueID();
      const deviceModel = DeviceInfo.getModel();

      console.log(user);
      console.log("device id" + deviceId);
      console.log("get fcm"+fcmtoken);
      db.collection("tokens").doc(fcmtoken+"_"+user.uid).set({
        uid: user.uid,
        name: user.displayName,
        fcm_token:fcmtoken,
        device_id: deviceId,
        device_Model: deviceModel,
        createTime: new Date()

      }).then((user) => {
        console.log("successfully add fcm ");
        this._addUser();
        this.loadFeaturedCategories();
        
      }).catch((error) => {
        console.log("user error" + error)
      })
    }
    else
    {
      this.loadFeaturedCategories();
    }
  }


  _addUser() {
    //this.setState({ isLoading: true });
    const { user } = this.props;
    var name="";
    if( user.displayName == undefined ||  user.displayName == null)
    {
      name="Customer";
    }
    else
    {
      name=user.displayName;
    }
    db.collection("users").doc(user.uid).set({
      firstname: name,
      lastname: "",
      email: user.email,
      phone: user.phoneNumber,
      imageLink: user.photoURL,
      //password:this.state.password,
      uid: user.uid,
      createdAt: user.creationTime,
      phVerify_status: 'N'
    }).then((user) => {
      //this.setState({ isLoading: false });
    }).catch((error) => {
      this.setState({ isLoading: false });
      console.log("user error" + error)
    })
  }




  _onLoadPage()
  {
    if(this.state.notif=="true")
    {
      AsyncStorage.setItem("notify","false");
      this.props.navigation.navigate("NewProject")
    }
    else
    {
      return false;
    }
  }

  onPressNotificacion() {
    this.props.navigation.navigate("NewProject");
  }

  // async componentWillMount()
  // {
  //   alert();
  //   const notify=await AsyncStorage.getItem("notify");
  //     alert(notify);
  //     if(notify=="true")
  //     {
  //       await AsyncStorage.setItem("notify", "false");
  //       this.props.navigation.navigate("NewProject")
  //     }
  // }

  loadFeaturedCategories = () => {
    const categories = featuredCategories.map(category => ({
      id: category.id,
      name: category.name,
      services: category.serviceIds
        .map(getServiceById)
        .filter(service => !!service)
    }));
    this.setState({
      featuredCategories: categories
    });
  };

  openSearch = () => {
    firebase.analytics().logEvent("open_search_bar");
    this.props.navigation.navigate("Search");
  };

  startNewProject = serviceId => {
    firebase
      .analytics()
      .logEvent("start_new_project", { serviceId: serviceId });
    console.log("Navigating to service:", serviceId);
    this.props.resetProject();
    this.props.setServiceId(serviceId);
    this.props.navigation.navigate("NewProject");
  };

  render() {
    return (
      <View style={styles.container}>
        <SearchBarButton
          style={styles.searchBarButton}
          onPress={this.openSearch}
        />
        <SafeAreaView>
          <FeaturedCategoryList
            style={styles.list}
            contentContainerStyle={styles.contentContainer}
            featuredCategories={this.state.featuredCategories}
            serviceSelected={this.startNewProject}
            // contentInsetAdjustmentBehavior="always"
          />
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchBar: { marginHorizontal: 8, marginTop: 8, elevation: 1 },
  searchBarCover: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 10000
  },
  searchBarButton: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1,
    backgroundColor: theme.colors.background
  },
  container: {
    backgroundColor: theme.colors.background,
    flex: 1
  },
  contentContainer: {
    paddingTop: 76
  },
  list: {
    zIndex: 0
  }
});

HomeScreen.propTypes = {};

export default connect(
  null,
  { setServiceId, resetProject }
)(HomeScreen);
