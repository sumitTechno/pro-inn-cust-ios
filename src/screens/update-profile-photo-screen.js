import React, { Component } from "react";
import { StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import ImagePicker from "react-native-image-picker";
import { Button } from "react-native-paper";
import FastImage from "react-native-fast-image";
import firebase from "react-native-firebase";

import NavigationHeader from "../components/navigation-header";
import globalStyles from "../global-styles";
import theme from "../theme";
import { resizeImageForUpload } from "../util/image";
import { uploadProfilePhoto } from "../api/storage";

const db = firebase.firestore();
const pickerOptions = {
  title: "Select Profile Photo",
  maxWidth : 300,
  maxHeight : 300,
  noData : true,
  quality : 0.5,
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

const Header = ({ navigation }) => (
  <NavigationHeader
    showBackButton={true}
    navigation={navigation}
    title="Update Profile Photo"
  />
);

class UpdateProfilePhotoScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: <Header navigation={navigation} />
  });

  state = {
    newImageSource: null,
    loading: false
  };

  componentDidMount() {
    firebase
      .analytics()
      .setCurrentScreen("UpdateProfilePhoto", "UpdateProfilePhoto");
  }

  showImagePicker = () => {
    this.setState({ loading: true });
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(pickerOptions, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
        this.setState({ loading: false });
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
        this.setState({ loading: false });
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState(
          {
            newImageSource: source
          },
          () => this.uploadPhoto()
        );
      }
    });
  };

  uploadPhoto = async () => {
    const { user } = this.props;
    console.log("uid"+user.uid);
    const { newImageSource } = this.state;
    //var rotation;
    // if (  newImageSource.originalRotation === 90 ) {
    //    rotation = 90
    // } else if (newImageSource.originalRotation === 270 ) {
    //    rotation = -90
    // }
    const fileRef = await uploadProfilePhoto(newImageSource.uri);
    await user.updateProfile({ photoURL: await fileRef.getDownloadURL() });
    console.log("photoURL"+user.photoURL);

    db.collection("users").doc(user.uid).update({
      imageLink: user.photoURL
    }).then((user) => {
      console.log("done update");
      this.setState({ loading: false, newImageSource: null });
    }).catch((error) => {
      this.setState({ loading: false, newImageSource: null });
      console.log("user error" + error);
    })

  };

  render() {
    const { user } = this.props;
    const { newImageSource, loading } = this.state;

    if (!user) {
      navigation.goBack(null);
    }

    return (
      <ScrollView
        style={globalStyles.surfaceContainer}
        contentContainerStyle={styles.contentContainer}
      >
        <FastImage
          source={
            newImageSource || (user.photoURL ? { uri: user.photoURL } : null)
          }
          style={styles.profilePhoto}
          resizeMode={FastImage.resizeMode.cover}
        />
        <Button
          loading={loading}
          color={theme.colors.primary}
          style={styles.selectButton}
          mode="outlined"
          onPress={this.showImagePicker}
        >
          Select a photo
        </Button>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    padding: 16,
    alignItems: "flex-start"
  },
  profilePhoto: {
    width: 128,
    height: 128,
    borderRadius: 128 / 2,
    backgroundColor: theme.colors.background,
    alignSelf: "center"
  },
  selectButton: {
    alignSelf: "center",
    marginTop: 16
  }
});

UpdateProfilePhotoScreen.propTypes = {};

const mapStateToProps = ({ auth }) => ({
  user: auth.user,
  email: auth.email
});
export default connect(mapStateToProps)(UpdateProfilePhotoScreen);
