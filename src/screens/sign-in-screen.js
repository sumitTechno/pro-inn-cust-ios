import React, { Component } from "react";
import { StyleSheet, ScrollView, Image, View } from "react-native";
import {
  Button,
  HelperText,
  Text,
  TextInput,
  Title,
  Appbar
} from "react-native-paper";
import { connect } from "react-redux";
import { SafeAreaView } from "react-navigation";
import bannerWhite from "../../assets/images/banner-white.png";
import theme from "../theme";
import { sendSignInLink } from "../api/sign-in";
import { isValidEmail } from "../util/validation";
import { setAuthEmail } from "../redux/reducers/auth.reducer";
import firebase from "react-native-firebase";

const Header = ({ navigation }) => (
  <SafeAreaView style={styles.header}>
    <Appbar.BackAction
      color={theme.colors.primary}
      onPress={() => navigation.goBack(null)}
    />
  </SafeAreaView>
);

class SignInScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: <Header navigation={navigation} />
  });

  state = {
    touched: false,
    loading: false
  };

  componentDidMount() {
    firebase.analytics().setCurrentScreen("SignIn", "SignIn");
    const { user, navigation } = this.props;
    if (user) {
      navigation.goBack(null);
    }
  }

  render() {
    const { setAuthEmail } = this.props;

    const showHelperText =
      this.state.touched && !isValidEmail(this.props.email || "");

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <Image style={styles.banner} source={bannerWhite} />
        <Title style={styles.title}>Sign In</Title>
        <Text style={styles.text}>
          Enter your email to sign in. If you do not already have an account,
          one will be created with the email provided.
        </Text>
        <TextInput
          mode={"flat"}
          style={styles.input}
          label="Email"
          value={this.props.email}
          onChangeText={email => setAuthEmail(email)}
          autoCapitalize="none"
          keyboardType="email-address"
          onBlur={() => this.setState({ touched: true })}
        />
        <HelperText type="error" visible={showHelperText}>
          Email address is invalid!
        </HelperText>
        <Button
          style={styles.button}
          onPress={this.sendSignInLink}
          loading={this.state.loading}
          mode="contained"
        >
          Continue
        </Button>
        <View style={styles.flex} />
      </ScrollView>
    );
  }

  sendSignInLink = async () => {
    const { email } = this.props;
    this.setState({ loading: true });
    if (!email || !isValidEmail(email)) {
      return;
    }
    await sendSignInLink(email);
    this.setState({ loading: false });
    this.props.navigation.navigate("SignInLoading");
  };
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: theme.colors.surface
  },
  container: {
    backgroundColor: theme.colors.surface,
    flex: 1
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingBottom: 16,
    alignItems: "flex-start",
    flex: 1
  },
  flex: {
    flex: 1
  },
  title: {
    marginBottom: 32,
    alignSelf: "center"
  },
  banner: {
    width: 150,
    height: 41,
    resizeMode: "contain",
    tintColor: theme.colors.primary,
    alignSelf: "center"
  },
  input: {
    alignSelf: "stretch"
  },
  button: {
    alignSelf: "flex-end"
  },
  text: {
    alignSelf: "flex-start",
    marginBottom: 16,
    color: theme.colors.textSecondary
  }
});

SignInScreen.propTypes = {};

const mapStateToProps = ({ auth }) => ({
  user: auth.user,
  email: auth.email
});
export default connect(
  mapStateToProps,
  { setAuthEmail }
)(SignInScreen);
