import React, { Component } from "react";
import { View, SafeAreaView, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { Searchbar } from "react-native-paper";
import globalStyles from "../global-styles";
import { updateSearchQuery } from "../redux/reducers/search.reducer";
import SearchResultList from "../components/search-result-list";
import {resetProject, setServiceId} from "../redux/reducers/new-project.reducer";
import firebase from "react-native-firebase";

const SearchBar = ({
  style,
  value,
  onChangeText,
  searchBarRef,
  onBlur,
  onClose
}) => (
  <SafeAreaView style={style}>
    <Searchbar
      style={styles.searchBar}
      placeholder="Search for a service"
      value={value}
      onChangeText={onChangeText}
      ref={searchBarRef}
      icon="arrow-back"
      onIconPress={onClose}
      onBlur={onBlur}
    />
  </SafeAreaView>
);

class SearchScreen extends Component {
  componentDidMount() {
    firebase.analytics().setCurrentScreen("Search", "Search");
    if (this.searchBar) {
      this.searchBar.focus();
    }
  }

  onClose = () => {
    this.props.navigation.goBack();
  };

  onBlur = () => {
    if (!this.props.query) {
      this.onClose();
    }
  };

  serviceSelected = serviceId => {
    this.props.resetProject();
    this.props.setServiceId(serviceId);
    this.props.navigation.navigate("NewProject");
  };

  render() {
    const { query, results, updateSearchQuery } = this.props;
    return (
      <View style={globalStyles.surfaceContainer}>
        <SearchBar
          searchBarRef={ref => (this.searchBar = ref)}
          onClose={this.onClose}
          onChangeText={updateSearchQuery}
          value={query}
          onBlur={this.onBlur}
        />
        <SearchResultList
          results={results}
          serviceSelected={this.serviceSelected}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchBar: {
    marginHorizontal: 8,
    marginTop: 8,
    elevation: 0,
    borderWidth: 1,
    borderColor: "rgba(0, 0, 0, .12)"
  }
});

SearchScreen.propTypes = {};

const mapStateToProps = ({ search }) => ({
  query: search.query,
  results: search.results
});
export default connect(
  mapStateToProps,
  { updateSearchQuery, setServiceId, resetProject }
)(SearchScreen);
