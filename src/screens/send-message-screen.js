import React, { Component } from "react";
import {
  SafeAreaView,
  ScrollView,
  Linking,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  AsyncStorage,
  Button,
  ActivityIndicator,
  Platform,
  Dimensions,
  ImageBackground,
  StatusBar,
  FlatList,
  Keyboard
} from "react-native";
import NavigationHeader from "../components/navigation-header";
import theme from "../theme";
import firebase from "react-native-firebase";
import handMessaging from "../../assets/images/hand-messaging.png";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import LoginPlaceholder from "../components/login-placeholder";
import { connect } from "react-redux";
import { CachedImage } from 'react-native-cached-image';
import Entypo from 'react-native-vector-icons/Entypo';
import { DotsLoader, RotationHoleLoader, CirclesLoader, BubblesLoader } from 'react-native-indicator';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import DeviceInfo from 'react-native-device-info';

const hasNotch = DeviceInfo.hasNotch(); 
const model = DeviceInfo.getModel();

const screenSize = Dimensions.get('window');
const db = firebase.firestore();


class MessagesScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null // <Header navigation={navigation} />
  });

  componentDidMount() {
    firebase.analytics().setCurrentScreen("SendMessages", "SendMessages");
  }

  openMailTo = () => {
    const subject = "Feedback or Suggestion";
    const body = "I have a suggestion...";
    const encode = text => text.replace("/s/", "%20");
    Linking.openURL(
      `mailto:feedback@proinyourpocket.com?subject=${encode(
        subject
      )}&body=${encode(body)}`
    );
  };

  state = {
    uid: this.props.user.uid,
    toChatId: this.props.navigation.state.params.toChatId,
    fromChatId: this.props.navigation.state.params.fromChatId,
    bidderName: this.props.navigation.state.params.bidderName,
    bidderBusinessName: this.props.navigation.state.params.bidderBusinessName,
    bidderImage: this.props.navigation.state.params.bidderImage,
    navigateFrom: this.props.navigation.state.params.navigateFrom,
    message_status: this.props.navigation.state.params.message_status,
    project_id: this.props.navigation.state.params.project_id,//new change

    show_archive: false,
    message: "",
    isLoading: false,
    isLoading1: true,
    chatArray: [],
    isLoadingMore: false,
    isarchive: false,
    reciver_message_status: "",
    chatArrayMod: [],
    lastPage: 5,
    totalNoFetch: 1,
    fetchCounter: 5,
    pageCounter: 1,
    isLoadingMore: false,
    chatArraySend:[],
    showChat:false
    //...project
  }

  componentDidMount() {
    console.log(this.state.toChatId);//to whom user message
    console.log(this.state.fromChatId);//from where the messegae sent
    console.log(this.state.uid);
    this._getMessages();
  }

  _showArchive() {
    if (this.state.show_archive == false) {
      this.setState({
        show_archive: true
      })
    }
    else {
      this.setState({
        show_archive: false
      })
    }

  }

  _getMessages() {
    var docRef = db.collection("messages").doc(this.state.uid).collection(this.state.message_status).doc(this.state.toChatId + "_" + this.state.project_id);

    if (this.state.navigateFrom.trim() == "project_details") {
      var docRef2 = db.collection("messages").doc(this.state.uid).collection("archive").doc(this.state.toChatId + "_" + this.state.project_id);
      //new change
      docRef2.get().then((doc) => {
        if (doc.exists) {
          var array = [];
          array = doc.data().chat;
          this.setState({
            chatArray: array,
            chatArrayMod: array,
            message_status: "archive"
          });
          console.log("chat array mod" + this.state.chatArrayMod);
        }
        else {
          docRef.get().then((doc) => {
            if (doc.exists) {
              var array = doc.data().chat;
              this.setState({
                chatArray: array,
                chatArrayMod: array
              });
              console.log("chat array mod" + this.state.chatArrayMod);
              console.log("chat array" + array);
            } else {
              // doc.data() will be undefined in this case
              console.log("No such document!");
            }
            this.setState({ isLoading1: false });
          }).catch((eror) => {
            this.setState({ isLoading1: false });
            console.log("Error getting document:", error);
          });
        }
        this.setState({ isLoading1: false });
      }).catch((eror) => {
        this.setState({ isLoading1: false });
        console.log("Error getting document:", error);
      });
    }

    else {
      docRef.get().then((doc) => {
        if (doc.exists) {
          var array = doc.data().chat;
          this.setState({
            chatArray: array,
            chatArrayMod: array
          });
          console.log("chat array mod" + this.state.chatArrayMod);
          console.log("chat array" + array);
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
        this.setState({ isLoading1: false });
      }).catch((eror) => {
        this.setState({ isLoading1: false });
        console.log("Error getting document:", error);
      });
    }

    this._checkReciverStatus();

  }


  _checkReciverStatus() {
    this.setState({ isLoading1: true });
    var docRef = db.collection("messages").doc(this.state.toChatId).collection("archive").doc(this.state.uid + "_" + this.state.project_id);
    //new change
    docRef.get().then((doc) => {
      if (doc.exists) {
        this.setState({
          reciver_message_status: "archive"
        });
      }
      else {
        this.setState({
          reciver_message_status: "inbox"
        });
      }
      this.setState({ isLoading1: false });
    }).catch((eror) => {
      this.setState({ isLoading1: false });
      console.log("Error getting document:", error);
    });
  }


  _getMessage_afterSend() {
    var docRef = db.collection("messages").doc(this.state.uid).collection(this.state.message_status).doc(this.state.toChatId + "_" + this.state.project_id);
    docRef.get().then((doc) => {
      if (doc.exists) {
        var array_mod = [];
        array_mod = doc.data().chat;
        console.log("length check" + array_mod.length);
        this.setState({
          chatArrayMod:array_mod,
          chatArray: array_mod,
          chatArraySend:array_mod,
          showChat:true
        }, this.showAlert.bind(this));
      }
      else {
        console.log("No such document!");
      }
    }).catch((eror) => {
      this.setState({isLoading: false });
      console.log("Error getting document:", error);
    });
  }

  showAlert() {
    this.setState({ isLoading: false })
  }

  _sendMessage() {

    this.setState({ isLoading: true });
    var message_id = this.state.fromChatId + "-" + this.state.toChatId;//customer-serviceprovider
    var obj = {};
    obj = {
      "message_id": message_id,
      "sender_id": this.state.fromChatId,
      "reciver_id": this.state.toChatId,
      "project_id": this.state.project_id,//new change
      "description": this.state.message,
      "status": "inbox",
      "createdAt": new Date()
    };
    var array = [];
    array = this.state.chatArray;
    array.unshift(obj);


    var navigationData = {
      "toChatId": this.state.uid,
      "fromChatId": this.state.toChatId,
      "ProjectOwnerName": this.props.user.displayName,
      "project_id": this.state.project_id,
      "imageLink": this.props.user.photoURL,
      "navigateFrom": "notification_message",
      "message_status": this.state.reciver_message_status,
      "Uid": this.state.toChatId,
    };



    db.collection("messages").doc(this.state.uid)
      .collection(this.state.message_status)
      .doc(this.state.toChatId + "_" + this.state.project_id)//new change
      .set({
        chat: array
      }).then((user) => {
        db.collection("messages").doc(this.state.toChatId)
          .collection(this.state.reciver_message_status)
          .doc(this.state.fromChatId + "_" + this.state.project_id)//new change
          .set({
            chat: array
          }).then((user) => {

            var msjFunction = db.collection("messages_function").doc(this.state.fromChatId + "_" + this.state.toChatId + "_" + this.state.project_id);

            msjFunction.delete().then((data) => {
              msjFunction
                .set({
                  reciver_id: this.state.toChatId,
                  sender_id: this.state.fromChatId,
                  reciver_status: "Pro",
                  message: this.state.message,
                  reciver_msj_status: this.state.reciver_message_status,
                  navigationData: navigationData,
                  create_message_time: new Date()
                }).then((user) => {
                  console.log("success");
                  this.inputMessage.setNativeProps({ text: "" });
                  this.setState({
                    message: "",
                  });
                  Keyboard.dismiss();
                  this._getMessage_afterSend();

                  //this.setState({ isLoading: false });
                }).catch((error) => {
                  this.setState({ isLoading: false });
                  console.log("user error" + error)
                })


            }).catch((error) => {
              this.setState({ isLoading: false });
              console.log("user error" + error)
            });
            // console.log("success");
            // this.setState({ isLoading: false });

          }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error)
          })


      }).catch((error) => {
        this.setState({ isLoading: false });
        console.log("user error" + error)
      })
  }





  _archiveChat() {
    this.setState({
      show_archive: false,
      isLoading1: true
    });


    db.collection("messages").doc(this.state.uid)//new change
      .collection("inbox").doc(this.state.toChatId + "_" + this.state.project_id).delete().then((data) =>
        db.collection("messages").doc(this.state.uid)
          .collection("archive")
          .doc(this.state.toChatId + "_" + this.state.project_id)//new change
          .set({
            chat: this.state.chatArray
          }).then((user) => {
            this.setState({
              isLoading1: false,
              //isarchive: true,
              message_status: "archive"
              // show_archive: false
            });
            this.props.navigation.replace("Messages");
          }).catch((error) => {
            this.setState({ isLoading1: false });
            console.log("user error" + error)
          }))
  }



  _goTo() {
    if (this.state.navigateFrom.trim() === "project_details") {
      this.props.navigation.goBack();
    }
    else {
      this.props.navigation.replace("Messages")
    }
  }

  render() {
    const { user } = this.props;
    if (!user) {
      return <LoginPlaceholder />;
    }

    return (
      <View style={styles.container}>

        <View style={[styles.topContainer, { zIndex: 0 }]}>
          <TouchableOpacity 
          onPress={() => 
             //this._goTo()
            this.props.navigation.replace("Messages")
          }
          
          >
            <Ionicons name="md-arrow-round-back" style={styles.backButton} />
          </TouchableOpacity>
          <View style={[styles.avatorContainer, { zIndex: 0 }]}>
            {(this.state.bidderImage == "" || this.state.bidderImage == undefined) ?
              (
                <Image
                  source={require('../../assets/images/user.png')}
                  style={styles.avatorImage}
                />
              )
              :
              (
                <CachedImage
                  source={{ uri: this.state.bidderImage }}
                  style={styles.avatorImage}
                />
              )}

            < View style={{ marginLeft: 10 }}>
              <Text style={styles.titleText}>{this.state.bidderName}
              </Text>



              <View style={{ flexDirection: "row", justifyContent: "space-between", width: screenSize.width - 130, }}>
                {/* <Text style={styles.timeText}>{this.state.createdAt}</Text> */}
                {/* <Text style={styles.timeText}>{this.state.createdAt}</Text> */}
                <Text style={styles.timeText}> {this.state.bidderBusinessName} </Text>
                {(this.state.message_status == "inbox") ?
                  (<TouchableOpacity
                    onPress={() => this._showArchive()}>
                    <Entypo name='dots-three-vertical' color='#969da8' size={30} />
                  </TouchableOpacity>)
                  :
                  null}

              </View>

              {(this.state.show_archive == true) ?
                (<TouchableOpacity
                  onPress={() => this._archiveChat()}
                  style={styles.archiveMenu}>
                  <Text style={{ color: "black", fontWeight: "600", fontSize: 16 }}>Archive</Text>
                </TouchableOpacity>)
                :
                null}
            </View>
          </View>


        </View>


        <FlatList
            inverted={true}
            refreshing={true}
            key={this.state.chatArrayMod.length}
            data={this.state.chatArrayMod}
            extraData={this.state}

            ListHeaderComponent={() =>
              <View style={{ height: 100 }}></View>
            }
            renderItem={({ item }) => (
              <View>
                {(item.sender_id == this.state.uid) ?
                  (<View style={[styles.senderMessageContainer]}>
                    <Text style={styles.sendMessageText}>{item.description}</Text>
                  </View>)
                  :
                  (<View style={styles.receiverMessageContainer}>
                    <Text style={styles.receiveMessageText}>{item.description}</Text>
                  </View>)}
              </View>
            )}
            keyExtractor={item => item.sender_id}
          />


        {
          (this.state.isLoading1 == true) ?
            (
              <View style={styles.activity_main}>
                <View style={styles.activity_sub}>
                  {/* <BubblesLoader size={50} dotRadius={5} color='black' /> */}
                </View>
              </View>
            )
            :
            (
              <View>
                {(this.state.isarchive == false) ?
                  (
                    <View style={styles.messageContainer}>
                      <TextInput
                        style={styles.textInput}
                        multiline={true}
                        //keyboardType='default'
                        underlineColorAndroid="transparent"
                        placeholder="Enter a message"
                        placeholderTextColor="#ababab"
                        returnKeyType='send'
                        autoCorrect={false}
                        autoCapitalize="sentences"
                        // onSubmitEditing={() => this.inputPassword.focus()}
                        onChangeText={(input) => this.setState({ message: input })}
                        ref={((input) => this.inputMessage = input)}
                      />
                      {(this.state.isLoading == true) ?
                        (<View style={{ padding: 5 }}>
                          <RotationHoleLoader size={10} rotationSpeed={800} strokeWidth={10} color='black' />
                        </View>)
                        :
                        null}

                      {(this.state.message.trim() != "") ?
                        (
                          <TouchableOpacity activeOpacity={0.9}
                            onPress={() => this._sendMessage()}>
                            <Text style={styles.sendText}>Send</Text>
                          </TouchableOpacity>)
                        :
                        (<View>
                          <Text style={styles.sendText}>Send</Text>
                        </View>
                        )}
                    </View>
                  )
                  :
                  null}
              </View>
            )
        }


          <KeyboardSpacer />

      </View >



    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },

  topContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    borderBottomColor: '#8e8e93',
    borderBottomWidth: 0.7,
    marginTop:25
    // position:"absolute",
    // top:0
  },

  backButton: {
    fontSize: 30,
    color: '#315cab',
    paddingLeft: 10
  },

  avatorImage: {
    width: 70,
    height: 70,
    borderRadius: 35
  },

  titleText: {
    //fontFamily: regularText,
    fontWeight: 'bold',
    color: '#1e1e1e',
    fontSize: 16,
    maxWidth: screenSize.width - 100
  },

  avatorContainer: {
    flexDirection: 'row',
    marginTop: 10,
    padding: 10
  },

  timeText: {
    //fontFamily: regularText,
    fontSize: 14,
    //color:"black",
    marginTop: 10
  },

  bidPlaceContainer: {
    paddingHorizontal: 10,
    borderTopColor: '#8e8e93',
    borderTopWidth: 0.7,
    borderBottomColor: '#8e8e93',
    borderBottomWidth: 0.7,
    paddingVertical: 15
  },

  bidText: {
    //fontFamily: italicText,
    fontSize: 15
  },

  footerContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    // alignItems : 'center',
    height: 60,
    backgroundColor: '#fff'
  },

  footerItem: {
    width: screenSize.width / 4,
    alignItems: 'center',
    justifyContent: 'center'
  },

  footerImage: {
    fontSize: 35
  },

  messageContainer: {
    borderTopColor: '#8e8e93',
    borderTopWidth: 0.5,
    borderBottomColor: '#8e8e93',
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    height :hasNotch?60: 50,
    marginBottom:hasNotch?10:0
  },

  textInput: {
    marginLeft: 10,
    //fontFamily: italicText,
    fontSize: 16,
    flex: 1
  },

  sendText: {
    //fontFamily : regularText,
    color: '#03a9f4',
    marginRight: 10,
    fontSize: 18
    // alignSelf: 'flex-end'
  },

  senderMessageContainer: {
    backgroundColor: '#03a9f4',
    marginTop: 8,
    marginBottom: 4,
    marginRight: 10,
    maxWidth: screenSize.width - 60,
    alignSelf: 'flex-end',
    borderRadius: 10
  },

  sendMessageText: {
    color: '#fff',
    //fontFamily : regularText,
    padding: 10
  },

  receiverMessageContainer: {
    backgroundColor: '#e5e5ea',
    marginTop: 8,
    marginBottom: 4,
    marginLeft: 10,
    maxWidth: screenSize.width - 60,
    alignSelf: 'flex-start',
    borderRadius: 10
  },

  receiveMessageText: {
    color: '#1e1e1e',
    //fontFamily : regularText,
    padding: 10
  },
  archiveMenu: {
    backgroundColor: "#ffff",
    width: 100,
    height: 50,
    position: "absolute",
    right: 30,
    top: 30,
    zIndex: 10,
    padding: 10,
    //elevation: 5,
    ...Platform.select({
      android: {
      elevation: 5,
      
      },
      ios: {
      shadowColor: '#000000',
      shadowOffset: {
      width: 0,
      height: 3
      },
      shadowRadius: 5,
      shadowOpacity: 1.0,
      },
      }),

    borderRadius: 5,
    justifyContent: "center",
    alignItems: 'center'
  },
  activity_main: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 20,
    //elevation: 5,
    ...Platform.select({
      android: { elevation: 5, },
      ios: {
        shadowColor: '#999',
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
      },
    }),
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
  },

  activity_sub: {
    position: 'absolute',
    top: screenSize.height / 2,
    alignSelf: 'center',
    justifyContent: "center",
    alignItems: 'center',
    zIndex: 20,
    //elevation:5,
    ...Platform.select({
      android: { elevation: 5, },
      ios: {
        shadowColor: '#999',
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
      },
    }),
  },
})

MessagesScreen.propTypes = {};
const mapStateToProps = ({ auth }) => ({
  user: auth.user
});
export default connect(mapStateToProps)(MessagesScreen);
