import React, { Component } from "react";
import { View, ActivityIndicator, StyleSheet, Image } from "react-native";
import globalStyles from "../global-styles";
import { Button, Text, Title } from "react-native-paper";
import projects from "../api/project-collection";
import { connect } from "react-redux";
import { resetProject,setProjectVerified } from "../redux/reducers/new-project.reducer";
import firebase from "react-native-firebase";
import theme from "../theme";
import {attachOwner} from '../api/attach-owner'

class CreateProjectLoadingScreen extends Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };

  state = {
    error: null
  };

  async componentDidMount() {
    firebase
      .analytics()
      .setCurrentScreen("CreateProjectLoading", "CreateProjectLoading");
    await this.createProject();
  }

  createProject = async () => {
    try {
      this.props.setProjectVerified('Y')
      await projects.verifyProject();
      this.props.resetProject();
      this.props.navigation.navigate("ProjectConfirmation");    
    } catch (err) {
      this.setState({
        error: "Uh oh! Something went wrong. Please try again later."
      });
    }
  };

  render() {
    return (
      <View style={[globalStyles.surfaceContainer, styles.centered]}>
        <View style={styles.row}>
          <Title style={styles.title}>Creating your project</Title>
          {!this.state.error && (
            <ActivityIndicator animating={true} color={theme.colors.disabled} />
          )}
        </View>
        {this.state.error && (
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>{this.state.error}</Text>
            <Button
              onPress={() => this.props.navigation.goBack()}
              mode="outlined"
              style={styles.backButton}
            >
              Go back
            </Button>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centered: {
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    marginRight: 16
  },
  image: {
    width: "50%",
    height: "50%",
    resizeMode: "contain"
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  errorContainer: {
    marginTop: 16,
    alignItems: "center"
  },
  errorText: {
    color: "firebrick",
    marginBottom: 16
  },
  backButton: {}
});

CreateProjectLoadingScreen.propTypes = {};

const mapStateToProps = ({ newProject }) => ({ newProject });

export default connect(
  mapStateToProps ,
  { resetProject,setProjectVerified }
)(CreateProjectLoadingScreen);
