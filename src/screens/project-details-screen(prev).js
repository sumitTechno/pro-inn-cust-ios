import React, { Component } from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Platform,
  ActivityIndicator,
  Modal
} from "react-native";
import { Title, Subheading, Caption, Text } from "react-native-paper";
import NavigationHeader from "../components/navigation-header";
import globalSyles from "../global-styles";
import { getServiceById } from "../api/services";
import Moment from "react-moment";
import firebase from "react-native-firebase";
import { SegmentedControls } from 'react-native-radio-buttons';
import { LinesLoader } from 'react-native-indicator';
import update from 'react-addons-update';
import { CachedImage } from 'react-native-cached-image';

const options = [
  "Active",
  "Hired",
  "Not Interested",
];

const optionsButton = [
  "Hire",
  "Not Interested",
];

const db = firebase.firestore();
const screenSize = Dimensions.get('window');
const Header = ({ navigation }) => (
  <NavigationHeader
    showBackButton={true}
    navigation={navigation}
    title="Project Details"
  />
);

class ProjectDetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <Header navigation={navigation} />
    };
  };

  state = {
    serviceName: "",
    createdAt: null,
    postalCode: "",
    description: "",
    Pimage: [],
    bidderList: [],
    selectedOption: 'Active',
    selectedOptionButton: 'Hire',
    bidderDetails: [],
    show_loader: false,
    hiredBidder: [],
    notInterestedBidder: [],
    isLoading: false,
    project_id: "",
    counter: 0,
    show_option: false,
    project_bid_length: 0,
    show_active: false,
    project: this.props.navigation.state.params.project,
  };

  componentDidMount() {
    firebase.analytics().setCurrentScreen("ProjectDetails", "ProjectDetails");
    const { navigation } = this.props;
    const { project } = navigation.state.params || {};
    const service = getServiceById(project.serviceId);
    this.setState({
      serviceName: service.name,
      ...project
    });
    var array = [];
    var arrayDetails = [];

    console.log("project" + JSON.stringify(project));
    console.log("bidderIdList" + project.bidderIdList);

    if (project.bidderIdList != undefined && project.bidderIdList.length > 0) {
      //alert("if");
      this.setState({
        show_loader: true,
        project_bid_length: project.bidderIdList.length
      });

      for (var i = 0; i < project.bidderIdList.length; i++) {

        var docRef = db.collection("place_bid").doc(project.id + "_" + project.bidderIdList[i]);
        this.setState({
          project_id: project.id
        })
        docRef.get().then((doc) => {
          if (doc.exists) {
            console.log("Document data:", doc.data());
            var obj_get = doc.data();
            var obj_mod = {
              show: false
            };
            var obj = Object.assign(obj_mod, obj_get);
            console.log("obj" + JSON.stringify(obj));
            array.push(obj);
            if (obj.projectStatus == "Pending" || obj.projectStatus == undefined) {
              this.state.bidderList.push(obj);
              this.setState({
                show_active: true
              });
            }
            else if (obj.projectStatus == "Hired" || obj.projectStatus == "Completed") {
              this.state.hiredBidder.push(obj);

            }
            else if (obj.projectStatus == "NI") {
              this.state.notInterestedBidder.push(obj);
            }
            console.log("array" + this.state.bidderList);
          }
        }).catch((error) => {
          this.setState({
            show_loader: false,
            isLoading: false,
          });
          console.log("Error getting document:", error);
        });
      }
      this.state.bidderList = this.state.bidderList.sort((a, b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));
      this.state.hiredBidder = this.state.hiredBidder.sort((a, b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));
      this.state.notInterestedBidder = this.state.notInterestedBidder.sort((a, b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));


      for (var i = 0; i < project.bidderIdList.length; i++) {
        var docRef2 = db.collection("users").doc(project.bidderIdList[i]);
        this._getDetails(docRef2, project.bidderIdList.length);
      }
    }
    else {
      //alert("else");
      this.setState({
        show_loader: false,
        isLoading: false
      })
    }
  }

  _getDetails(docRef2, len) {
    //alert();
    console.log("length" + len);
    console.log("get details");
    this.setState({
      show_loader: true
    })
    console.log(len);
    var arrayDetails = [];
    console.log("doc");
    docRef2.get().then((doc) => {
      if (doc.exists) {
        console.log("Document data:", doc.data());
        var obj = doc.data();
        console.log("obj" + JSON.stringify(obj));
        arrayDetails.push(obj);
        this.state.bidderDetails.push(obj);
        console.log("array details" + this.state.bidderDetails);
        console.log("bidder details" + this.state.bidderDetails.length);
        console.log("length" + len);

        if (this.state.bidderDetails.length == len) {
          this.setState({
            show_loader: false,
            //selectedOption: 'Active',

          })
        }
      }
    }).catch((error) => {
      this.setState({
        show_loader: false,
        isLoading: false
      });
      console.log("Error getting document:", error);
    });
  }

  setSelectedOption(selectedOption) {
    if (selectedOption.trim() == "Active") {
      this.setState({
        show_active: true,
        selectedOption: selectedOption
        //show_active:false
      });
    }
    else {
      this.setState({
        show_active: false,
        selectedOption: selectedOption
      });
    }

  }

  // setSelectedOptionButton(selectedOption) {
  //   this.setState({
  //     selectedOptionButton: selectedOption
  //   }, this.checkOption.bind(this));
  // }

  // checkOption() {
  //   if (this.state.selectedOptionButton == "Hire") {
  //     this._callHire();
  //   }
  //   else {
  //     this._callNotInterest();
  //   }
  // }

  _getName(item) {
    var dateObject = new Date(Date.parse(item.createdAt));
    var dateReadable = dateObject.toDateString().trim();
    //var docRef = db.collection("users").doc(item.bidderId);
    var name = this._bidName(item.bidderId);
    console.log("get name" + name);
    return (
      <View style={{ marginLeft: 10 }}>
        <Text style={styles.titleText}>{name} &nbsp;
                  <Text style={{ fontWeight: '100' }}>Placed a bid for </Text>&nbsp;
                                                   $ {item.bidPrice}
        </Text>
        <Text style={styles.timeText}>{dateReadable}</Text>
      </View>
    )

  }

  _bidName(id) {

    // console.log("id"+id);
    console.log("details" + JSON.stringify(this.state.bidderDetails));
    var name = "";
    for (var i = 0; i < this.state.bidderDetails.length; i++) {
      console.log("uid" + this.state.bidderDetails[i].uid + "id" + id);
      if (this.state.bidderDetails[i].uid == id) {
        console.log("get name" + this.state.bidderDetails[i].firstname);
        console.log("get name" + this.state.bidderDetails[i].lastname);
        name = this.state.bidderDetails[i].firstname + " " + this.state.bidderDetails[i].lastname;
      }
    }
    console.log("get name" + name);
    return name;
  }

  _getUserImage(item) {
    url = this._bidderImage(item.bidderId);
    if (url == "" || url == undefined) {
      return (
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("AccountDetailsReview", { item: item })}
        >
          <Image
            source={require('../../assets/images/user.png')}
            style={styles.avatorImage}
          />
        </TouchableOpacity>
      )
    }
    else {
      return (
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("AccountDetailsReview", { item: item })}
        >
          <CachedImage
            source={{ uri: url }}
            style={styles.avatorImage}
          />
        </TouchableOpacity>
      )
    }

  }

  _bidderImage(id) {
    var url = "";
    for (var i = 0; i < this.state.bidderDetails.length; i++) {
      console.log("uid" + this.state.bidderDetails[i].uid);
      if (this.state.bidderDetails[i].uid == id) {
        url = this.state.bidderDetails[i].imageLink;
      }
    }
    return url;
  }

  _callHire(index, id, type) {
    console.log(index);
    if (type == "hire") {
      var newArray = this.state.bidderList.slice();
      for (var i = 0; i < newArray.length; i++) {
        if (id == newArray[i].bidderId) {
          console.log("true");
          newArray[i] = update(newArray[i], { $merge: { show: newArray[i].show == false ? true : false } });
        }
      }
      console.log("new data array" + JSON.stringify(newArray));
      this.setState({
        bidderList: newArray,
      })
    }
    else if (type == "NI") {
      var newArray = this.state.notInterestedBidder.slice();
      for (var i = 0; i < newArray.length; i++) {
        if (id == newArray[i].bidderId) {
          console.log("true");
          newArray[i] = update(newArray[i], { $merge: { show: newArray[i].show == false ? true : false } });
        }
      }
      console.log("new data array" + JSON.stringify(newArray));
      this.setState({
        notInterestedBidder: newArray,
      })
    }



    // if (type == "hire") {
    //   this._hireBidder(id);
    // }
  }

  _onConfirmHire(index, id, type) {
    this._callHire(index, id, type);
    this._hireBidder(id, type);
    //this._callHire(index,id,type);
  }

  _hireBidder(id, type) {

    console.log("hire bidder" + this.state.project_id);
    //console.log("id")
    this.setState({ isLoading: true });
    //this._callHire(index, id, "cancel");
    db.collection("place_bid").doc(this.state.project_id + "_" + id)
      .update({
        projectStatus: "Hired",
      }).then((user) => {
        db.collection("projects").doc(this.state.project_id)
          .update({
            projectStatus: "Hired",
            hiredId: id
          }).then((user) => {
            this.updateHire(id, type);

          }).catch((error) => {
            this.setState({
              isLoading: false,
            });
            console.log("user error" + error);
          })

        //this.updateHire(id, type);

      }).catch((error) => {
        this.setState({
          isLoading: false,
        });
        console.log("user error" + error);
      })

  }

  updateHire(id, type) {
    if (type == "hire") {
      for (var i = 0; i < this.state.bidderList.length; i++) {
        if (this.state.bidderList[i].bidderId === id) {
          console.log("bidder list id" + this.state.bidderList[i].id);
          console.log("hire id" + id);
          this.state.hiredBidder.push(this.state.bidderList[i]);
          this.state.bidderList.splice(i, 1);
        }
      }
      this._lostPending();
      this._lostNI();

      this.setState({
        show_active: false,
        selectedOption: "Hired",
        isLoading: false,
      })
      console.log("hired list" + this.state.hiredBidder);
      console.log("bidder list" + this.state.bidderList);
    }
    else if (type == "NI") {
      for (var i = 0; i < this.state.notInterestedBidder.length; i++) {
        if (this.state.notInterestedBidder[i].bidderId === id) {
          console.log("bidder list id" + this.state.notInterestedBidder[i].id);
          console.log("hire id" + id);
          this.state.hiredBidder.push(this.state.notInterestedBidder[i]);
          this.state.notInterestedBidder.splice(i, 1);
        }
      }
      this.setState({
        selectedOption: "Hired",
        isLoading: false
      })
      console.log("hired list" + this.state.hiredBidder);
      console.log("bidder list" + this.state.bidderList);
    }

  }

  _notInterest(index, id) {
    this.setState({ isLoading: true });
    //this._callHire(index, id, "cancel");
    db.collection("place_bid").doc(this.state.project_id + "_" + id)
      .update({
        projectStatus: "NI"
      }).then((user) => {
        for (var i = 0; i < this.state.bidderList.length; i++) {
          if (this.state.bidderList[i].bidderId === id) {
            console.log("bidder list id" + this.state.bidderList[i].id);
            console.log("hire id" + id);
            this.state.notInterestedBidder.push(this.state.bidderList[i]);
            this.state.bidderList.splice(i, 1);
          }
        }
        this.setState({
          show_active: false,
          selectedOption: "Not Interested",
          isLoading: false
        })
        console.log("hired list" + this.state.notInterestedBidder);
        console.log("bidder list" + this.state.bidderList);

      }).catch((error) => {
        this.setState({ isLoading: false });
        console.log("user error" + error);
      })
  }

  _completeProject(index, id) {
    this.setState({ isLoading: true });
    var newArray = this.state.hiredBidder.slice();
    db.collection("place_bid").doc(this.state.project_id + "_" + id)
      .update({
        projectStatus: "Completed"
      }).then((user) => {
        db.collection("projects").doc(this.state.project_id)
          .update({
            projectStatus: "Completed",
            completedBy: "owner",
            hiredId: id
          }).then((user) => {
            console.log("done complete");
            this._lostPending();
            this._lostNI();
            for (var i = 0; i < newArray.length; i++) {
              if (id == newArray[i].bidderId) {
                console.log("true");
                newArray[i] = update(newArray[i], { $merge: { projectStatus: "Completed" } });
              }
            }
            console.log("new completed data array" + JSON.stringify(newArray));
            this.setState({
              hiredBidder: newArray,
              selectedOption: "Hired",
              isLoading: false
            })
          }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error);
          })
      }).catch((error) => {
        this.setState({ isLoading: false });
        console.log("user error" + error);
      })
  }

  _lostPending() {
    console.log("lost pensing");
    for (var i = 0; i < this.state.bidderList.length; i++) {
      db.collection("place_bid").doc(this.state.project_id + "_" + this.state.bidderList[i].bidderId)
        .update({
          projectStatus: "Lost"
        }).then((user) => {
          this.setState({
            bidderList: []
          })
        }).catch((error) => {
          this.setState({ isLoading: false });
          console.log("user error for pending" + error);
        })
    }
  }

  _lostNI() {
    console.log("lost ni");
    for (var i = 0; i < this.state.notInterestedBidder.length; i++) {
      db.collection("place_bid").doc(this.state.project_id + "_" + this.state.notInterestedBidder[i].bidderId)
        .update({
          projectStatus: "Lost"
        }).then((user) => {
          this.setState({
            notInterestedBidder: []
          })
        }).catch((error) => {
          this.setState({ isLoading: false });
          console.log("user error for lostni" + error);
        })
    }
  }


  _getBusinessHireDetails(item, serviceName) {
    var name = this._bidName(item.bidderId);
    var business_name = this._bidderBusiness(item.bidderId);

    return (
      <Text style={{ color: 'black', fontSize: 15 }}>Are you sure you want to hire {name} with {business_name} for your {serviceName} project?</Text>
    )

  }


  _bidderBusiness(id) {

    // console.log("id"+id);
    console.log("details" + JSON.stringify(this.state.bidderDetails));
    var businessName = "";
    for (var i = 0; i < this.state.bidderDetails.length; i++) {
      console.log("uid" + this.state.bidderDetails[i].uid + "id" + id);
      if (this.state.bidderDetails[i].uid == id) {
        if (this.state.bidderDetails[i].BusinessInfo.CName != undefined) {
          console.log("get name" + this.state.bidderDetails[i].BusinessInfo.CName);
          businessName = this.state.bidderDetails[i].BusinessInfo.CName;
        }

      }
    }
    console.log("get name" + businessName);
    return businessName;
  }

  _goToChat(item, bidderId, serviceName, time) {

    console.log("item" + item);
    console.log("from chat id" + item.ownerId);
    console.log("to chat id" + bidderId);


    var dateObject = new Date(Date.parse(item.createdAt));
    var dateReadable = dateObject.toDateString().trim();
    var name = this._bidName(item.bidderId);
    var business_name = this._bidderBusiness(item.bidderId);
    var url = this._bidderImage(bidderId);

    this.props.navigation.navigate("SendMessages",
      {
        toChatId: bidderId,
        fromChatId: item.ownerId,
        bidderName: name,
        bidderBusinessName: business_name,
        project_id: this.state.project_id,
        bidderImage: url,
        navigateFrom: "project_details",
        message_status: "inbox"
        //project:project
      });
  }

  render() {
    const { serviceName, postalCode, createdAt, description, Pimage } = this.state;

    return (
      <View
        style={[globalSyles.surfaceContainer]}>
        {(this.state.show_loader == true) ?
          (<View style={styles.activity_sub}>
            <ActivityIndicator
              size="large"
              color='#D0D3D4'
              style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
            />
          </View>)
          :
          (
            <ScrollView
              contentContainerStyle={styles.contentContainer}
            >

              <View style={styles.descriptionView}>
                {(Pimage.length > 0 || Pimage == null) ?
                  (<View>
                    {/* <Caption style={styles.caption}>Project Image </Caption> */}
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                      <View style={{ marginBottom: 10, flexDirection: "row" }}>
                        {
                          Pimage.map((item) => (
                            <View style={{ width: 300, height: 150, marginRight: 10, borderColor: "#9d9d9d", borderWidth: 2, justifyContent: "center", alignItems: "center" }}>
                              <CachedImage
                                source={{ uri: item }}
                                style={{ width: 300, height: 150, borderColor: "#9d9d9d", borderWidth: 1 }}
                              />
                            </View>
                          ))
                        }
                      </View>
                    </ScrollView>
                  </View>)
                  : null}

                <Title>{serviceName}</Title>
                {/* <Subheading>
          {postalCode} -{" "}
          <Moment element={Subheading} fromNow>
            {createdAt}
          </Moment>
        </Subheading> */}
                <Text style={[styles.text]}>{postalCode} (Postal Code) {" "} - {" "}
                  <Moment element={Text} style={styles.text} fromNow>
                    {createdAt}
                  </Moment>
                </Text>
                <Subheading style={{ marginTop: 15 }}>{description}</Subheading>
              </View>

              {(this.state.project_bid_length > 0) ?
                (<View style={[styles.descriptionView, { marginTop: 10 }]}>
                  <SegmentedControls
                    tint={'#03a9f4'}
                    selectedTint={'white'}
                    backTint={'#fff'}
                    options={options}
                    allowFontScaling={false} // default: true
                    onSelection={this.setSelectedOption.bind(this)}
                    selectedOption={this.state.selectedOption}
                    optionStyle={{ fontSize: 16 }}
                    optionContainerStyle={{ flex: 1 }}
                  />
                </View>)
                :
                null}

              {(this.state.show_active == true) ?
                (
                  <FlatList
                    data={this.state.bidderList}
                    renderItem={({ item, index }) =>
                      (
                        <View style={[styles.descriptionView, { marginTop: 10 }]}>
                          <View style={{ flexDirection: 'row' }}>
                            {this._getUserImage(item)}
                            {this._getName(item)}
                            {/* {this._show(item)} */}
                          </View>
                          <View style={{
                            marginTop: 20,
                            width: screenSize.width - 32,
                            alignSelf: "center",
                            //borderColor: "#03a9f4",
                            //borderWidth: 1,
                            height: 40,
                            borderRadius: 10,
                            flexDirection: "row",
                            justifyContent: "space-between"
                          }}>

                            <TouchableOpacity
                              onPress={() => this._callHire(index, item.bidderId, "hire")}
                              style={{
                                width: (screenSize.width - 32) / 3 - 5,
                                justifyContent: "center",
                                alignItems: "center",
                                backgroundColor: "#03a9f4",
                                borderRadius: 5
                              }}>
                              <Text style={{ color: "#ffff", fontSize: 16 }}>Hire</Text>
                            </TouchableOpacity>


                            <TouchableOpacity
                              onPress={() => this._goToChat(item, item.bidderId, serviceName, item.createdAt)}
                              style={{
                                width: (screenSize.width - 32) / 3 - 5,
                                justifyContent: "center",
                                alignItems: "center",
                                backgroundColor: "#03a9f4",
                                borderRadius: 5
                              }}>
                              <Text style={{ color: "#ffff", fontSize: 16 }}>Message</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                              onPress={() => this._notInterest(index, item.bidderId)}
                              style={{
                                width: (screenSize.width - 32) / 3 - 5,
                                justifyContent: "center",
                                alignItems: "center",
                                backgroundColor: "#e2dddd",
                                borderRadius: 5
                              }}>
                              <Text style={{ color: "black", fontSize: 16 }}>Not Interested</Text>
                            </TouchableOpacity>



                          </View>

                          {(item.show == true) ?
                            (
                              <Modal
                                animationType="slide"
                                transparent={false}
                                visible={item.show}
                                onRequestClose={() => this.setModalVisible(false)}
                                style={{ height: 300, width: 300 }}
                                transparent={true}>
                                <View style={styles.modalview}>
                                  <View style={styles.modalviewoptions}>
                                    <View style={[styles.modalviewoptions1]}>
                                      <Text style={{ color: '#999999', fontSize: 12 }}>{serviceName}</Text>
                                    </View>
                                    <View style={[styles.modalviewoptions2, styles.commonstyleoption]}>
                                      <TouchableOpacity style={{ padding: 30 }}>
                                        {/* <Text style={{ color: 'black', fontSize: 15 }}>Are you sure to hire this bidder for {serviceName} ?</Text> */}

                                        {this._getBusinessHireDetails(item, serviceName)}
                                      </TouchableOpacity>
                                    </View>
                                    <View style={[styles.modalviewoptions4, styles.commonstyleoption]}>
                                      <TouchableOpacity
                                        onPress={() => this._onConfirmHire(index, item.bidderId, "hire")}
                                        style={{ padding: 15 }} >
                                        <Text style={{ color: '#003fff', fontSize: 19, }}>Confirm</Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                  <View style={styles.modalviewcancel}>
                                    <TouchableOpacity onPress={() => this._callHire(index, item.bidderId, "hire")} style={{ padding: 15 }}>
                                      <Text style={{ fontSize: 20, color: "red" }}>Cancel</Text>
                                    </TouchableOpacity>
                                  </View>
                                </View>
                              </Modal>
                            ) :
                            null}
                        </View>
                      )}
                    keyExtractor={item => item.bidderId}
                  />)
                :
                null}

              {(this.state.selectedOption == "Hired") ?
                (
                  <FlatList
                    data={this.state.hiredBidder}
                    renderItem={({ item, index }) =>
                      (
                        <View style={[styles.descriptionView, { marginTop: 10 }]}>
                          <View style={{ flexDirection: 'row' }}>
                            {this._getUserImage(item)}
                            {this._getName(item)}
                            {/* {this._show(item)} */}
                          </View>
                          <View style={{
                            marginTop: 20,
                            width: screenSize.width - 32,
                            alignSelf: "center",
                            //borderColor: "#03a9f4",
                            //borderWidth: 1,
                            height: 40,
                            borderRadius: 10,
                            flexDirection: "row",
                            justifyContent: "space-between"
                          }}>


                            {(item.reviewStatus == undefined)?
                            (<View style={{ flexDirection: "row", width: screenSize.width - 32, justifyContent: "space-between" }}>
                              {(item.projectStatus != "Completed") ?
                                (<TouchableOpacity
                                  onPress={() => this._completeProject(index, item.bidderId, "hire")}
                                  style={{
                                    width: (screenSize.width - 32) / 2,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: "#03a9f4",
                                    borderRadius: 5
                                  }}>
                                  <Text style={{ color: "#ffff", fontSize: 16 }}>Complete Project</Text>
                                </TouchableOpacity>)
                                :
                                (

                                     <TouchableOpacity
                                        //onPress={()=>alert(item.reviewStatus)}
                                       onPress={() => this.props.navigation.replace("Review", { item: item, project: this.state.project })}
                                        style={{
                                          width: (screenSize.width - 32) / 2,
                                          justifyContent: "center",
                                          alignItems: "center",
                                          backgroundColor: "#03a9f4",
                                          borderRadius: 5
                                        }}>
                                        <Text style={{ color: "#ffff", fontSize: 16 }}>Leave a review</Text>
                                      </TouchableOpacity>

                                )}

                              <TouchableOpacity
                                onPress={() => this._goToChat(item, item.bidderId, serviceName, item.createdAt)}
                                style={{
                                  width: (screenSize.width - 32) / 2 - 5,
                                  justifyContent: "center",
                                  alignItems: "center",
                                  backgroundColor: "#e2dddd",
                                  borderRadius: 5
                                }}>
                                <Text style={{ color: "black", fontSize: 16 }}>Send Message</Text>
                              </TouchableOpacity>

                            </View>)
                            :
                            (
                            <View style={{ flexDirection: "row", width: screenSize.width - 32, justifyContent: "center",alignItems:"center" }}>
                              <TouchableOpacity
                                onPress={() => this._goToChat(item, item.bidderId, serviceName, item.createdAt)}
                                style={{
                                  width: (screenSize.width - 32),
                                  height:50,
                                  justifyContent: "center",
                                  alignItems: "center",
                                  backgroundColor: "#03a9f4",
                                  borderRadius: 5
                                }}>
                                <Text style={{ color: "#ffff", fontSize: 16 }}>Send Message</Text>
                              </TouchableOpacity>

                            </View>
                            )}


                          </View>

                          {(item.show == true) ?
                            (
                              <Modal
                                animationType="slide"
                                transparent={false}
                                visible={item.show}
                                onRequestClose={() => this.setModalVisible(false)}
                                style={{ height: 300, width: 300 }}
                                transparent={true}>
                                <View style={styles.modalview}>
                                  <View style={styles.modalviewoptions}>
                                    <View style={[styles.modalviewoptions1]}>
                                      <Text style={{ color: '#999999', fontSize: 12 }}>{serviceName}</Text>
                                    </View>
                                    <View style={[styles.modalviewoptions2, styles.commonstyleoption]}>
                                      <TouchableOpacity style={{ padding: 30 }}>
                                        {/* <Text style={{ color: 'black', fontSize: 15 }}>Are you sure to hire this bidder for {serviceName} ?</Text> */}
                                        {this._getBusinessHireDetails(item, serviceName)}
                                      </TouchableOpacity>
                                    </View>
                                    <View style={[styles.modalviewoptions4, styles.commonstyleoption]}>
                                      <TouchableOpacity
                                        //onPress={() => this._confirmHire("NI")}
                                        style={{ padding: 15 }} >
                                        <Text style={{ color: '#003fff', fontSize: 19, }}>Confirm</Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                  <View style={styles.modalviewcancel}>
                                    <TouchableOpacity onPress={() => this._callHire(index, item.bidderId, "cancel")} style={{ padding: 15 }}>
                                      <Text style={{ fontSize: 20, color: "red" }}>Cancel</Text>
                                    </TouchableOpacity>
                                  </View>
                                </View>
                              </Modal>
                            ) :
                            null}
                        </View>
                      )}
                    keyExtractor={item => item.bidderId}
                  />
                )
                :
                null}

              {(this.state.selectedOption == "Not Interested") ?
                (
                  <FlatList
                    data={this.state.notInterestedBidder}
                    renderItem={({ item, index }) =>
                      (
                        <View style={[styles.descriptionView, { marginTop: 10 }]}>
                          <View style={{ flexDirection: 'row' }}>
                            {this._getUserImage(item)}
                            {this._getName(item)}
                            {/* {this._show(item)} */}
                          </View>
                          <View style={{
                            marginTop: 20,
                            width: screenSize.width - 32,
                            alignSelf: "center",
                            //borderColor: "#03a9f4",
                            //borderWidth: 1,
                            height: 40,
                            borderRadius: 10,
                            flexDirection: "row",
                            justifyContent: "space-between"
                          }}>

                            <TouchableOpacity
                              onPress={() => this._callHire(index, item.bidderId, "NI")}
                              style={{
                                width: (screenSize.width - 32) / 2,
                                justifyContent: "center",
                                alignItems: "center",
                                backgroundColor: "#03a9f4",
                                borderRadius: 5
                              }}>
                              <Text style={{ color: "#ffff", fontSize: 16 }}>Hire</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                              onPress={() => this._goToChat(item, item.bidderId, serviceName, item.createdAt)}
                              style={{
                                width: (screenSize.width - 32) / 2 - 5,
                                justifyContent: "center",
                                alignItems: "center",
                                backgroundColor: "#e2dddd",
                                borderRadius: 5
                              }}>
                              <Text style={{ color: "black", fontSize: 16 }}>Send Message</Text>
                            </TouchableOpacity>

                          </View>

                          {(item.show == true) ?
                            (
                              <Modal
                                animationType="slide"
                                transparent={false}
                                visible={item.show}
                                onRequestClose={() => this.setModalVisible(false)}
                                style={{ height: 300, width: 300 }}
                                transparent={true}>
                                <View style={styles.modalview}>
                                  <View style={styles.modalviewoptions}>
                                    <View style={[styles.modalviewoptions1]}>
                                      <Text style={{ color: '#999999', fontSize: 12 }}>{serviceName}</Text>
                                    </View>
                                    <View style={[styles.modalviewoptions2, styles.commonstyleoption]}>
                                      <TouchableOpacity style={{ padding: 30 }}>
                                        {/* <Text style={{ color: 'black', fontSize: 15 }}>Are you sure to hire this bidder for {serviceName} ?</Text> */}
                                        {this._getBusinessHireDetails(item, serviceName)}
                                      </TouchableOpacity>
                                    </View>
                                    <View style={[styles.modalviewoptions4, styles.commonstyleoption]}>
                                      <TouchableOpacity
                                        onPress={() => this._onConfirmHire(index, item.bidderId, "NI")}
                                        style={{ padding: 15 }} >
                                        <Text style={{ color: '#003fff', fontSize: 19, }}>Confirm</Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                  <View style={styles.modalviewcancel}>
                                    <TouchableOpacity onPress={() => this._callHire(index, item.bidderId, "NI")} style={{ padding: 15 }}>
                                      <Text style={{ fontSize: 20, color: "red" }}>Cancel</Text>
                                    </TouchableOpacity>
                                  </View>
                                </View>
                              </Modal>
                            ) :
                            null}
                        </View>
                      )}
                    keyExtractor={item => item.bidderId}
                  />
                )
                :
                null}
            </ScrollView>

          )
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    //padding: 16
    //backgroundColor:"#fcfcfc"
  },
  caption: {
    fontWeight: "500",
    marginTop: 16
  },
  descriptionView: {
    backgroundColor: "#ffff",
    elevation: 5,
    padding: 16,
    width: screenSize.width,
    //borderBottomColor:"#b9b9b9",
    //borderBottomWidth:2
  },
  text: {
    color: "#a09a9a"
  },
  avatorImage: {
    width: 70,
    height: 70,
    borderRadius: 35
  },
  titleText: {
    //fontFamily: regularText,
    fontWeight: 'bold',
    color: '#1e1e1e',
    fontSize: 16,
    maxWidth: screenSize.width - 96
  },

  timeText: {
    //fontFamily: regularText,
    fontSize: 16,
    marginTop: 10
  },

  descriptionText: {
    //fontFamily: regularText,
    fontSize: 16,
    marginVertical: 10,
    color: '#1e1e1e'
  },
  clickToUpload: {
    //elevation: 1,
    marginTop: 20,
    backgroundColor: "blue",
    borderColor: "#d2d1d1",
    justifyContent: "center",
    alignItems: "center",
    padding: 5,
    width: 100,
    height: 50
  },
  activity_sub: {
    position: 'absolute',
    top: screenSize.height / 2,
    backgroundColor: 'black',
    width: 50,
    alignSelf: 'center',
    justifyContent: "center",
    alignItems: 'center',
    zIndex: 10,
    //elevation:5,
    ...Platform.select({
      android: { elevation: 5, },
      ios: {
        shadowColor: '#999',
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
      },
    }),
    height: 50,
    borderRadius: 10
  },
  modalviewcancel: {
    alignItems: 'center',
    justifyContent: 'center',
    width: screenSize.width - 30,
    height: 60,
    borderRadius: 15,
    marginLeft: 15,
    position: 'absolute',
    bottom: 80,
    backgroundColor: 'white'
  },
  modalview: {
    height: screenSize.height,
    width: screenSize.width,
    backgroundColor: 'rgba(200, 200, 200, .6)',
  },
  modalviewoptions: {
    width: screenSize.width - 30,
    //height: 225,
    marginLeft: 15,
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 150,
    borderRadius: 15
  },
  commonstyleoption: {
    alignItems: 'center',
    justifyContent: 'center',
    //height: 60,
    borderTopWidth: 1,
    borderTopColor: '#cccccc'
  },
  modalviewoptions1: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
  },
  modalviewoptions2: {
    alignItems: 'center',
    justifyContent: 'center',
    //height:120,
  }
});

ProjectDetailsScreen.propTypes = {};

export default ProjectDetailsScreen;
