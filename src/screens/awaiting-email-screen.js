import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Platform,
  Linking,
  AsyncStorage,
  Dimensions,
  ActivityIndicator
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Button, Subheading, Title } from "react-native-paper";
import { connect } from "react-redux";

import theme from "../theme";
import NavigationHeader from "../components/navigation-header";
import firebase from "react-native-firebase";
import DeviceInfo from 'react-native-device-info';

const screenSize = Dimensions.get('window');
const db = firebase.firestore();
const Header = ({ navigation }) => (
  <NavigationHeader navigation={navigation} title="New Project" />
);

const Strong = ({ children }) => (
  <Subheading style={styles.bold}>{children}</Subheading>
);

class AwaitingEmailScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: <Header navigation={navigation} />
  });

  state = {
    nameLoading: false,
    isLoading: false,
  };

  async componentDidMount() {
    if (this.props.user) {
      await this.checkUser();
    }
  }

  async componentDidUpdate(prevProps) {
    if (!prevProps.user && !!this.props.user) {
      await this.checkUser();
    }
  }

  checkUser = async () => {
    //alert('err');
    this.setState({ nameLoading: true });
    try {
      await this.props.user.updateProfile({ displayName: this.props.name });
      await this._addFcmToken();

    } catch (err) {

      console.log("Failed to update user's name:", err);
    }
    this.props.navigation.navigate("CreateProjectLoading");
  };


  _addFcmToken = async () => {
    const { user } = this.props;
    this.setState({ nameLoading: true });
    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          AsyncStorage.setItem('fcm_token', fcmToken);
          console.log("fcm token" + fcmToken);

          if (user != null) {
            const deviceId = DeviceInfo.getUniqueID();
            const deviceModel = DeviceInfo.getModel();

            console.log(user);
            console.log("device id" + deviceId);

            db.collection("tokens").doc(fcmToken + "_" + user.uid).set({
              uid: user.uid,
              name: user.displayName,
              fcm_token: fcmToken,
              device_id: deviceId,
              device_Model: deviceModel,
              createTime: new Date()
            }).then((user) => {
              console.log("successfully add fcm ");
              this._addUser();
            }).catch((error) => {
              this.setState({ isLoading: false });
              console.log("user error" + error)
            })
          }
        }
        else {
          this.setState({ isLoading: false, nameLoading: false });
        }
      });
  }

  _addUser() {
    this.setState({ isLoading: true });
    const { user } = this.props;
    var name = "";
    if (user.displayName == undefined || user.displayName == null) {
      name = "Customer";
    }
    else {
      name = user.displayName;
    }
    db.collection("users").doc(user.uid).update({
      firstname: name,
      lastname: "",
      email: user.email,
      phone: user.phoneNumber,
      imageLink: user.photoURL,
      userType: 'CUSTOMER',
      uid: user.uid,
      createdAt: user.creationTime,
      phVerify_status: 'N'
    }).then((user) => {
      this.setState({ isLoading: false });
    }).catch((error) => {
      db.collection("users").doc(user.uid).set({
        firstname: name,
        lastname: "",
        email: user.email,
        phone: user.phoneNumber,
        imageLink: user.photoURL,
        userType: 'CUSTOMER',
        uid: user.uid,
        createdAt: user.creationTime,
        phVerify_status: 'N'
      }).then((data) => {
        this.setState({ isLoading: false });
      })
    })
  }




  render() {
    const { nameLoading } = this.state;
    const { email, authLoading } = this.props;
    const loading = authLoading || nameLoading;
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <Title style={styles.condensed}>Almost Done!</Title>
        <View style={styles.iconContainer}>
          <MaterialIcons name="email" size={48} color={theme.colors.primary} />
        </View>
        <Subheading style={styles.text}>
          We sent an email to <Strong>{email}</Strong>. Please open this email{" "}
          <Strong>on this device</Strong> to confirm your account and sign in.
        </Subheading>
        {!loading &&
          Platform.OS === "ios" && (
            <Button mode="outlined" onPress={this.openEmailClient}>
              Open Mail
            </Button>
          )}
        {loading && (
          <ActivityIndicator
            size="large"
            color={theme.colors.accent}
            animating={true}
          />
        )}

        {(this.state.isLoading == true) ?
          (<View style={styles.activity_sub}>
            <ActivityIndicator
              size="large"
              color='#D0D3D4'
              style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
            />
          </View>)
          :
          null}

      </ScrollView>
    );
  }

  openEmailClient = async () => {
    if (Platform.OS === `ios`) {
      const inbox = await Linking.canOpenURL(`inbox-gmail:`);

      // if inbox is installed, open that
      if (inbox) {
        Linking.openURL(`inbox-gmail:`);
      } else {
        // else default to iOS Mail app
        Linking.openURL(`message:`);
      }
    }
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.surface,
    flex: 1
  },
  contentContainer: {
    alignItems: "center",
    padding: 16,
    flex: 1
  },
  iconContainer: {
    width: 64,
    height: 64,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 16
  },
  condensed: {
    marginBottom: 16,
    fontWeight: '900',
    fontSize: 18
  },
  bold: {
    fontWeight: "700"
  },
  text: {
    marginBottom: 16
  },
  activity_sub: {
    position: 'absolute',
    top: screenSize.height / 2,
    backgroundColor: 'black',
    width: 50,
    alignSelf: 'center',
    justifyContent: "center",
    alignItems: 'center',
    zIndex: 10,
    //elevation:5,
    ...Platform.select({
      android: { elevation: 5, },
      ios: {
        shadowColor: '#999',
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
      },
    }),
    height: 50,
    borderRadius: 10
  },
});

AwaitingEmailScreen.propTypes = {};

const mapStateToProps = ({ auth }) => ({
  email: auth.email,
  user: auth.user,
  authLoading: auth.loading,
  name: auth.name
});

export default connect(mapStateToProps)(AwaitingEmailScreen);
