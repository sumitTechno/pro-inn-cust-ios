import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  SafeAreaView,
  AsyncStorage,
  Dimensions,
  Platform,
  ActivityIndicator
} from "react-native";
import { Subheading, Title, List, Text } from "react-native-paper";
import firebase from "react-native-firebase";
import { connect } from "react-redux";
import FastImage from "react-native-fast-image";

import LoginPlaceholder from "../components/login-placeholder";
import NavigationHeader from "../components/navigation-header";
import theme from "../theme";
import bannerWhite from "../../assets/images/banner-white.png";
import DeviceInfo from 'react-native-device-info';

const screenSize = Dimensions.get('window');
const Header = ({ navigation }) => (
  <NavigationHeader
    showBackButton={false}
    navigation={navigation}
    title={
      <Image
        source={bannerWhite}
        style={{
          width: "40%",
          height: 36,
          resizeMode: "contain",
          tintColor: theme.colors.primary
        }}
      />
    }
  />
);

const db = firebase.firestore();

class AccountScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null //<Header navigation={navigation} />
  });

  state = {
    loading: false,
    isLoading: false,
    fcm_token: ''
  };


  async  componentDidMount() {

   

    const { user } = this.props;

    if (this.props.user) {
      //await this._addUser();
      await this._addFcmToken();
    }
    

     firebase.analytics().setCurrentScreen("Account", "Account");
  
  }

  async  componentWillMount()
  {
     if (this.props.user) {
      await this._addFcmToken();
      //await this._addUser();
    }
  }

  async componentDidUpdate(prevProps) {
    // If we signed in, then check user and navigate.
    if (!prevProps.user && !!this.props.user) {
     await this._addFcmToken();
     // await this._addUser();
    }
  }




  _addFcmToken = async () => 
  {
    const { user } = this.props;
    this.setState({ isLoading: true });
    firebase.messaging().getToken()
    .then(fcmToken => {
      if (fcmToken) {
        AsyncStorage.setItem('fcm_token', fcmToken);
        console.log("fcm token" + fcmToken);

        if (user != null) {
          const deviceId = DeviceInfo.getUniqueID();
          const deviceModel = DeviceInfo.getModel();

          console.log(user);
          console.log("device id" + deviceId);

          db.collection("tokens").doc(fcmToken+"_"+user.uid).set({
            uid: user.uid,
            name: user.displayName,
            fcm_token:fcmToken,
            device_id: deviceId,
            device_Model: deviceModel,
            createTime:new Date()

          }).then((user) => {
            console.log("successfully add fcm ");
            this._addUser();
          }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error)
          })
        }
      }
      else {
        this._addUser();
      }
    });
  }


  _addUser() {
    this.setState({ isLoading: true });
    const { user } = this.props;
    var name="";
    if( user.displayName == undefined ||  user.displayName == null)
    {
      name="Customer";
    }
    else
    {
      name=user.displayName;
    }
    db.collection("users").doc(user.uid).update({
      firstname: name,
      lastname: "",
      email: user.email,
      phone: user.phoneNumber,
      imageLink: user.photoURL,
      userType:'CUSTOMER',
      uid: user.uid,
      createdAt: user.creationTime,
      phVerify_status: 'N'
    }).then((user) => {
      this.setState({ isLoading: false });
    }).catch((error) => {
      db.collection("users").doc(user.uid).set({
        firstname: name,
        lastname: "",
        email: user.email,
        phone: user.phoneNumber,
        imageLink: user.photoURL,
        //password:this.state.password,
        uid: user.uid,
        userType:'CUSTOMER',
        createdAt: user.creationTime,
        phVerify_status: 'N'
      }).then((data) => {
        //alert("hi");
        this.setState({ isLoading: false });
      })
    })
  }




  signOut = async () => {
    //alert("signout");
    try {
      await firebase.auth().signOut();
      //AsyncStorage.setItem('fcm_token',"");
    } catch (err) { }
  };

  sigOutFinal() {
    const { user } = this.props;
    db.collection("tokens").doc(this.state.fcm_token).delete().
      then((success) => {
        console.log("Document successfully deleted!");
        this.signOut();
      }).catch((error) => {
        this.setState({ isLoading: false });
        console.error("Error removing document: ", error);
      });
  }

  openPhotoUpdateModal = () => {
    this.props.navigation.navigate("UpdateProfilePhoto");
  };

  render() {
    const { user } = this.props;

    if (!user) {
      return <LoginPlaceholder />;
    }

    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <View style={styles.upperSection}>
            <View style={styles.row}>
              <FastImage
                style={styles.profileImage}
                source={user.photoURL ? { uri: user.photoURL } : null}
                resizeMode={FastImage.resizeMode.cover}
              />
              <View>
                <Title style={styles.condensed}>
                  {user.displayName || "No name set"}
                </Title>
                <Subheading style={styles.condensed}>{user.email}</Subheading>
              </View>
            </View>
            <TouchableOpacity onPress={this.openPhotoUpdateModal}>
              <Text style={styles.updatePhotoText}>Update profile photo</Text>
            </TouchableOpacity>
          </View>
          <List.Item
            onPress={() => this.props.navigation.navigate("AccountDetails")}
            style={styles.listItem}
            title={`Manage account details`}
            left={props => <List.Icon {...props} icon="settings" />}
          />
          <List.Item
            onPress={this.signOut}
            style={styles.listItem}
            title={`Sign out of ${user.email}`}
            left={props => <List.Icon {...props} icon="exit-to-app" />}
          />
        </ScrollView>
        {(this.state.isLoading == true) ?
          (<View style={styles.activity_sub}>
            <ActivityIndicator
              size="large"
              color='#D0D3D4'
              style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
            />
          </View>)
          :
          null}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.surface,
    flex: 1
  },
  contentContainer: {
    backgroundColor: theme.colors.background,
    flex: 1
  },
  upperSection: {
    padding: 16,
    backgroundColor: theme.colors.surface,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0, 0, 0, .12)"
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  profileImage: {
    width: 56,
    height: 56,
    borderRadius: 28,
    overflow: "hidden",
    backgroundColor: theme.colors.background,
    marginRight: 16
  },
  condensed: {
    lineHeight: 20
  },
  listItem: {
    padding: 0
  },
  updatePhotoText: {
    color: theme.colors.primary,
    marginTop: 16,
    marginLeft: 56 + 16
  },
  activity_sub: {
    position: 'absolute',
    top: screenSize.height / 2,
    backgroundColor: 'black',
    width: 50,
    alignSelf: 'center',
    justifyContent: "center",
    alignItems: 'center',
    zIndex: 10,
    //elevation:5,
    ...Platform.select({
      android: { elevation: 5, },
      ios: {
        shadowColor: '#999',
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
      },
    }),
    height: 50,
    borderRadius: 10
  },
});

AccountScreen.propTypes = {};

const mapStateToProps = ({ auth }) => ({
  user: auth.user
});
export default connect(mapStateToProps)(AccountScreen);
