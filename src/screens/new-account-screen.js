import React, { Component } from "react";
import { StyleSheet, Platform } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Subheading, Caption, Button } from "react-native-paper";
import firebase from "react-native-firebase";
import { connect } from "react-redux";

import IconTextInput from "../components/icon-text-input";
import NavigationHeader from "../components/navigation-header";
import globalStyles from "../global-styles";
import { isValidEmail } from "../util/validation";
import { setAuthEmail, setAuthName, forceSignIn } from "../redux/reducers/auth.reducer";
import AsyncHeaderAction from "../components/async-header-action";
import { sendSignInLink } from "../api/sign-in";
import projects from "../api/project-collection";
import { attachOwner } from '../api/attach-owner'
const Header = ({ navigation, done, disabled, loading }) => (
  <NavigationHeader
    navigation={navigation}
    title="New Project"
    actions={[
      <AsyncHeaderAction
        loading={loading}
      />
    ]}
  />
);

class NewAccountScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state || {};
    const { done, doneDisabled, loading } = params || {};
    return {
      header: (
        <Header
          navigation={navigation}
          loading={loading}
        />
      )
    };
  };



  componentDidMount() {
    const { params } = this.props.navigation.state || {};

    firebase.analytics().setCurrentScreen("NewAccount", "NewAccount");
    this.props.navigation.setParams({
      done: this.createAccount,
      doneDisabled: true,
      loading: false
    });
    if (this.nameInput && Platform.OS === "ios") {
      this.nameInput.focus();
    }
    this.updateValidity();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.email !== this.props.email ||
      prevProps.name !== this.props.name
    ) {
      this.updateValidity();
    }
  }

  updateValidity = () => {
    const { email, name, navigation } = this.props;
    const valid = !!name && !!email && isValidEmail(email);
    navigation.setParams({
      doneDisabled: !valid
    });
  };

  createAccount = async () => {
    firebase
      .analytics()
      .logEvent("create_new_account", { serviceId: this.props.serviceId });
    const { email } = this.props;
    this.props.navigation.setParams({
      loading: true
    });
    await sendSignInLink(email);
    // await projects.addOwnerToProject();
    if (this.props.navigation.state.params.comingFrom == 'createProject') {
      this.apiForceCreateUser(this.props.name, this.props.email)
    }
  };
  apiForceCreateUser(name, email) {
    return fetch('https://us-central1-pro-in-your-pocket.cloudfunctions.net/createCustomer',
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: name,
          email: email,
          token: 'pro_in_your_pocket_api_2020'
        })
      })
      .then(response => response.json())
      .then(async responsejson => {
        console.log(responsejson);
        if (responsejson.status == true) {
          //attach owner to project
          await projects.addOwnerToProject(responsejson.data);
          await this.props.navigation.setParams({
            loading: false
          });
          this.props.navigation.navigate("AwaitingEmail");

        }
        if (!responsejson.status && responsejson.error.code == 'auth/email-already-exists') {
          //normal flow
          await this.props.navigation.setParams({
            loading: false
          });
          this.props.navigation.navigate("AwaitingEmail");
        }
      })
  }

  render() {
    const { setAuthEmail, setAuthName, email, name } = this.props;
    return (
      <KeyboardAwareScrollView
        style={globalStyles.surfaceContainer}
        contentContainerStyle={styles.contentContainer}
      >
        <Subheading style={styles.prompt}>
          What is your name and email?
        </Subheading>
        <IconTextInput
          icon="person"
          value={name || ""}
          onChangeText={setAuthName}
          label="Your name"
          style={styles.input}
          inputRef={ref => (this.nameInput = ref)}
          autoCapitalize="words"
        />
        <IconTextInput
          icon="email"
          value={email || ""}
          onChangeText={setAuthEmail}
          label="Your email"
          style={styles.input}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Caption>
          We will use this information to contact you regarding your project.
          Your information will not be shared with pros.
        </Caption>
        <Button loading={this.props.navigation.state.loading} style={{ marginTop: 30 }} mode="contained" onPress={() => this.createAccount()}>
          Send Veification Email
        </Button>

      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    padding: 16
  },
  prompt: {
    marginBottom: 16
  },
  input: {
    marginBottom: 16
  }
});

NewAccountScreen.propTypes = {};

const mapStateToProps = ({ newProject, auth }) => ({
  newProject: newProject,
  email: auth.email,
  name: auth.name
});
export default connect(
  mapStateToProps,
  { setAuthEmail, setAuthName, forceSignIn }
)(NewAccountScreen);
