
import React, { Component } from "react";
import {
    SafeAreaView,
    ScrollView,
    Linking,
    View,
    Text,
    Dimensions,
    FlatList,
    Platform,
    ActivityIndicator,
    RefreshControl,
    TouchableOpacity,
    Image
} from "react-native";
import NavigationHeader from "../components/navigation-header";
import theme from "../theme";
import { Title, Subheading, Card, Appbar, TextInput } from "react-native-paper";
import firebase from "react-native-firebase";
import handMessaging from "../../assets/images/hand-messaging.png";
import globalStyles from "../global-styles";
import LoginPlaceholder from "../components/login-placeholder";
import StarRating from 'react-native-star-rating';
import { connect } from "react-redux";
import { CachedImage } from 'react-native-cached-image';

const db = firebase.firestore();
const screenSize = Dimensions.get('window');

const Header = ({ navigation }) => (
    <SafeAreaView style={styles.header}>
        <Appbar.BackAction
            color={theme.colors.primary}
            onPress={() => navigation.goBack(null)}
        />
    </SafeAreaView>
);

class ReviewScreen extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <Header navigation={navigation} />

    });

    state = {
        item: this.props.navigation.state.params.item,
        project: this.props.navigation.state.params.project,
        starCount: 0,
        reviewText: '',
        uid: '',
        isLoading: false,
        profileName: 'Pro',
        profileImage: null,

    };

    onStarRatingPress(rating) {
        console.log(rating);
        this.setState({
            starCount: rating
        });
    }

    componentDidMount() {
        if (this.props.user) {
            if (this.props.user.uid) {
                this.setState({
                    uid: this.props.user.uid
                });
            }
        }
        firebase.analytics().setCurrentScreen("AccountDetailsReview", "AccountDetailsReview");
        this._getProfileDetails(this.props.navigation.state.params.item.bidderId)
    }

    _showDate() {
        var dateValue = new Date(this.state.item.createdTimeStamp)
        //console.log(dateValue)
        var hours = dateValue.getHours();
        var minutes = dateValue.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        hours = hours < 10 ? '0' + hours : hours;
        var strTime = hours + ':' + minutes + ' ' + ampm;

        var dateReadable = dateValue.toDateString().trim();



        return (
            <Text style={{ paddingTop: 5 }}>{dateReadable} {strTime}</Text>

        )
    }


    _sendReview() {
        //alert();
        console.log(this.state.formReviewId);
        var d = new Date();
        var ts = d.valueOf();
        if (this.state.starCount == 0) {
            //CommonTasks._displayToast("Please give your rating");
            alert('Please give your rating')
            return false;
        }
        if (this.state.reviewText == "") {
            // CommonTasks._displayToast("Please leave a message");
            alert('Please leave a message')
            return false;
        }
       // alert();
        this.setState({ isLoading: true });
        db.collection("customerReviews").doc(this.state.item.projectId + "_" + this.state.item.ownerId).set({
            projectId: this.state.item.projectId,
            projectDescription: this.state.item.description,
            serviceId: this.state.item.serviceId,
            reviewer: this.state.item.ownerId,
            pro: this.state.item.bidderId,
            rating: this.state.starCount,
            review: this.state.reviewText,
            timeStamp: ts,
        }).then((success) => {
            db.collection("place_bid").doc(this.state.item.projectId+"_"+this.state.item.bidderId)
                .update({
                    reviewStatus: "Done",
                    review: this.state.reviewText,
                }).then((user) => {
                    this.setState({ isLoading: false });
                    //this.props.navigation.goBack();
                    this.props.navigation.replace("ProjectDetails",{ project:this.state.project });
                }).
                catch((error) => {
                    this.setState({ isLoading: false });
                    console.log("user error" + error)
                })


        }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error)
        })
    }

    _getProfileDetails(id) {
        this.setState({ isLoading: true })

        db.collection("users")
            .doc(id)
            .get()
            .then((doc) => {
                console.log('User data : ')
                console.log(doc.data());
                var fname = doc.data().firstname;
                var lname = doc.data().lastname;
                var name
                if (fname == '') {
                    name = 'Pro';
                }
                else {
                    if (lname == '') {
                        name = fname;
                    }
                    else {
                        name = fname + ' ' + lname;
                    }
                }
                this.setState({ profileImage: doc.data().imageLink, profileName: name, });
                this._getReviews(id)
            })
            .catch((err) => {
                this.setState({ isLoading: false });
                console.log(err)
            })
    }

    render() {
        const { user } = this.props;
        if (!user) {
            return <LoginPlaceholder />;
        }

        return (
            <View style={[globalStyles.surfaceContainer, { zIndex: 10 }]}>
                <View style={{ alignSelf: "center", marginBottom: 10, marginTop: -40, zIndex: 10 }}>
                    <Title>Review</Title>
                </View>
                <View style={styles.avatorContainer}>
                    <Image
                        source={this.state.profileImage ? {
                            uri:
                                this.state.profileImage
                        } :
                            require('../../assets/images/user.png')}
                        style={styles.avatorImage}
                    />
                    <View style={{ height: 90, width: screenSize.width - 100, marginLeft: 10, justifyContent: 'center' }}>
                        <View style={{ maxHeight: 65 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{this.state.profileName}
                                <Text style={{ fontWeight: 'normal' }}> completed </Text>
                                {this.state.item.description}
                            </Text>
                        </View>
                        {this._showDate()}
                    </View>
                </View>
                <View style={{ padding: 10 }}>
                    <Text>Rate your Pro</Text>
                    <StarRating
                        emptyStar={'ios-star-outline'}
                        fullStar={'ios-star'}
                        halfStar={'ios-star-half'}
                        iconSet={'Ionicons'}
                        maxStars={5}
                        rating={this.state.starCount}
                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                        fullStarColor={'#03a9f4'}
                        emptyStarColor={'#03a9f4'}
                        starSize={35}
                        starStyle={{ paddingLeft: 2 }}
                        containerStyle={{ width: 100, paddingTop: 2 }}
                    />
                </View>
                <ScrollView style={{ padding: 10, width: screenSize.width, borderBottomWidth: 0 }}>
                    <View style={{ alignItems: 'center' }}>
                        <TextInput style={{ width: screenSize.width - 20, backgroundColor: '#fff', borderBottomWidth: 0 }}
                            multiline={true}
                            //underlineColorAndroid='#fff)'
                            underlineColorAndroid="transparent"
                            onChangeText={(text) => (text)}
                            keyboardType='default'
                            value={this.state.reviewText}
                            placeholder="Write a Review"
                            placeholderTextColor="#ababab"
                            autoCorrect={false}
                            autoCapitalize="sentences"
                            autoComplete='off'
                            onChangeText={(input) => this.setState({ reviewText: input })}
                            ref={((input) => this.inputMessage = input)}>
                        </TextInput>
                        <TouchableOpacity onPress={() => this._sendReview()}>
                            <Text style={styles.reviewButton}>Leave Review</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>


                {(this.state.isLoading == true) ?
                    (<View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View>)
                    :
                    null}
            </View>
        );
    }
}

const styles = {
    avatorContainer: {
        width: screenSize.width,
        height: 90,
        backgroundColor: 'white',
        elevation: 3,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    reviewButton: {
        backgroundColor: theme.colors.primary,
        fontSize: 22,
        color: '#fff',
        padding: 10,
        borderRadius: 20,
        paddingHorizontal: 30,
        marginTop: 30,
        opacity: 0.9
    },
    container: {
        flex: 1,
        backgroundColor: theme.colors.background
    },
    header: {
        backgroundColor: theme.colors.surface
    },
    contentContainer: {
        padding: 16,
        alignItems: "center"
    },
    email: {
        color: theme.colors.primary
    },
    title: {
        alignSelf: "center"
    },
    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2.5,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },

};

ReviewScreen.propTypes = {};
const mapStateToProps = ({ auth }) => ({
    user: auth.user
});
export default connect(mapStateToProps)(ReviewScreen);
