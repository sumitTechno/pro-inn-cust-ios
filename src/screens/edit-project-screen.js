import React, { Component } from "react";
import {
    StyleSheet,
    Platform,
    View,
    Dimensions,
    TouchableOpacity,
    ProgressBarAndroid,
    Image,
    FlatList,
    ScrollView,
    ActivityIndicator,
    Text,
    Alert,
    SafeAreaView
} from "react-native";
import { HelperText, Subheading, Title, Button } from "react-native-paper";
import { connect } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { BubblesLoader } from 'react-native-indicator';

import NavigationHeader from "../components/navigation-header";
import globalStyles from "../global-styles";
import IconTextInput from "../components/icon-text-input";

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { LinesLoader } from 'react-native-indicator';

import HeaderAction from "../components/header-action";
import { getServiceById } from "../api/services";
import firebase from "react-native-firebase";
import { isValidPostalCode } from "../util/validation";
import ImagePicker from 'react-native-image-picker';
import RNRestart from 'react-native-restart';
import { resizeImageForUpload } from "../util/image";
import ImageResizer from "react-native-image-resizer";
import SelectImageBtn from "../components/select-image-btn";
import MyStatusBar from '../components/cross-platform-statusbar';

const db = firebase.firestore();
const screenSize = Dimensions.get('window');
const { width, height } = Dimensions.get('window');


const Header = ({ navigation, onComplete }) => (
    <NavigationHeader
        navigation={navigation}
        title="Edit Project"
        actions={[
            <HeaderAction />
        ]}
    />
);



export default class EditProjectScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        //header: null
        const { params } = navigation.state || {};
        const { onComplete } = params || {};

        return {
            header: <Header navigation={navigation} title="Edit Project" onComplete={onComplete} />
        };
    };

    state = {
        serviceName: "",
        postalCodeError: false,
        descriptionError: null,
        Progress_Value: 0.2,
        show_upload: false,
        show_loader: false,
        avatarSource: "",
        postalCode: '',
        description: '',
        Projectimages: [],
        projectId: this.props.navigation.state.params.projectId,
        isLoading: false,
        projectStatus: 'InProgress',
    };

    componentDidMount() {
        this.setState({ isLoading: true })
        firebase.analytics().setCurrentScreen("EditProject", "EditProject");
        const { navigation } = this.props;
        navigation.setParams({
            onComplete: this.submitProject
        });

        if (this.postalCodeInput && Platform.OS === "ios") {
            this.postalCodeInput.focus();
        }
        this.getProjectInfo();
    }
    getProjectInfo() {
        db.collection("projects").doc(this.props.navigation.state.params.projectId).get().then(doc => {
            if (doc.exists) {
                var project = doc.data();
                this.setState({
                    postalCode: project.postalCode,
                    description: project.description,
                    Projectimages: project.Pimage ? project.Pimage : [],
                    serviceName: getServiceById(project.serviceId),
                    isLoading: false,
                    projectStatus: project.projectStatus
                });
            }
        })
    }

    componentDidUpdate() {
        let keyboardScrollView = this.refs.KeyboardAwareScrollView;

        if (keyboardScrollView) keyboardScrollView.update();
    }

    _clickToUpload() {
        this.getImage();
    }

    getImage() {
        const options = {
            title: 'Select Picture',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                //alert((response.fileSize / 1048576).toFixed(3));
                var resizedUri = "";
                var filename = "";
                const source = { uri: response.uri };
                var rotation;
                if (response.originalRotation === 90) {
                    rotation = 90
                } else if (response.originalRotation === 270) {
                    rotation = -90
                }
                ImageResizer.createResizedImage(response.uri, 1024, 1024, "JPEG", 80, rotation).
                    then((responseImage) => {
                        console.log("image resizer" + JSON.stringify(responseImage));
                        resizedUri = responseImage.uri;
                        this.setState({
                            show_upload: true,
                            avatarSource: source,
                            show_loader: true
                        });

                        this._imageUp(responseImage.uri, responseImage.name);
                    }).catch((err) => {
                        console.log("err" + err);
                    });

            }
        });

    }



    _imageUp(uri, name) {
        this.setState({ show_loader: true })
        var id = this.state.id;
        firebase.storage().ref('user-image').child(name)
            .put(uri, { contentType: 'image/jpeg' }) //--> here just pass a uri
            .then((snapshot) => {
                console.log(JSON.stringify(snapshot.metadata));

                firebase.storage().ref('user-image').child(name).getDownloadURL()
                    .then((url) => {
                        var test = url;
                        console.log("download" + url);
                        var array = this.state.Projectimages;
                        array.push(url);
                        this.setState({ Projectimages: array });
                        this.Start_Progress();
                    })

                    .catch((error) => {
                        this.setState({ show_loader: false });
                        console.log("err download" + error);
                    });
            })
            .catch((error) => {
                this.setState({ show_loader: false });
                console.log("error" + error);
            });
    }


    Start_Progress = () => {

        this.value = setInterval(() => {
            this.setState({ Progress_Value: this.state.Progress_Value + .01 })
        }, 100);

        this.setState({
            show_upload: false,
            Progress_Value: 0.00
        })


        console.log("image array" + this.state.Projectimages);
        console.log("progress value" + this.state.Progress_Value);

    }
    removeImageConfirmation(value) {
        Alert.alert(
            'Delete image?',
            'Are you sure to delete this image from project?',
            [
                { text: 'Delete', onPress: () => this._delImage(value) },
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                }
            ],
            { cancelable: false },
        );
    }

    _delImage(value) {
        const currTime = new Date().toISOString();
        var array = this.state.Projectimages;
        array.forEach(function (element, i, object) {
            if (element.trim() == value.trim()) {
                object.splice(i, 1);
            }
        });
        this.setState({
            Projectimages: array
        })
        db.collection("projects").doc(this.state.projectId).update({
            Pimage: array,
            updatedAt: currTime,
        }).then((user) => alert("Image deleted successfully!")).catch((error) => {
            alert("Something went wrong!");
        })
    }

    submitProject = () => {
        const currTime = new Date().toISOString();
        if (!isValidPostalCode(this.state.postalCode)) {
            this.setState({
                postalCodeError: true,
            });
            return;
        }
        this.setState({
            postalCodeError: false,
            isLoading: true
        });
        db.collection("projects").doc(this.state.projectId).update({
            postalCode: this.state.postalCode,
            description: this.state.description,
            Pimage: this.state.Projectimages,
            updatedAt: currTime,
            isLoading: false,
            projectVerified:'Y'
        }).then((user) => this.props.navigation.goBack()).catch((error) => {
            alert("Something went wrong!");
            this.props.navigation.goBack()
        })
    };
    deleteProject() {
        Alert.alert(
            'Delete Project?',
            'Are you sure to delete this project?',
            [
                { text: 'Delete', onPress: () => this.apiDeleteProject() },
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                }
            ],
            { cancelable: false },
        );
    }
    apiDeleteProject() {
        this.setState({ isLoading: true });
        const deletedAt = new Date().toISOString();
        db.collection("projects").doc(this.state.projectId).update({
            projectStatus: 'Deleted',
            prevStatus: this.state.projectStatus,
            updatedAt: deletedAt,
        }).then((user) => {
            this.setState({ isLoading: false });
            alert("Project Deleted Successfuly!");
            this.props.navigation.goBack()
        }).catch((error) => {
            this.setState({ isLoading: false });
            alert("Something went wrong!");
        })

    }
    render() {

        return (

            <SafeAreaView style={[globalStyles.surfaceContainer, { zIndex: 10 }]}>
                <KeyboardAwareScrollView
                    extraScrollHeight={100}
                    ref="KeyboardAwareScrollView"
                    contentContainerStyle={styles.contentContainer}
                >


                    <View style={{ alignSelf: "center" }}>
                        <Title>{this.state.serviceName.name}</Title>
                    </View>

                    <View style={{ marginTop: 10, alignSelf: "center" }}>
                        <Subheading style={styles.prompt}>
                            Please provide your postal code and a brief description of what you
                            need done.
            </Subheading>
                    </View>

                    <IconTextInput
                        icon="place"
                        value={this.state.postalCode || ""}
                        onChangeText={txt => this.setState({ postalCode: txt })}
                        label="Postal code"
                        style={styles.input}
                        inputRef={ref => (this.postalCodeInput = ref)}
                        keyboardType="number-pad"
                    />
                    {this.state.postalCodeError && (
                        <HelperText
                            type="error"
                            style={styles.helperText}
                            visible={this.state.postalCodeError}
                        >
                            Please enter a valid postal code (e.g. 95014)
          </HelperText>
                    )}
                    <IconTextInput
                        icon="edit"
                        value={this.state.description || ""}
                        onChangeText={txt => this.setState({ description: txt })}
                        label="A short description of your project"
                        multiline={true}
                        style={styles.input}
                    />


                    {(this.state.Projectimages.length < 5) ?
                        (<View style={[styles.ImgaeUploadView, { marginBottom: 10, }]}>

                            <SelectImageBtn onClick={() => this._clickToUpload()} />

                            {(this.state.show_upload == true) ?
                                (<View style={{ flexDirection: "row" }}>

                                    <View style={{ width: 50, height: 50 }}>
                                        <Image
                                            source={this.state.avatarSource}
                                            style={{ width: 50, height: 50 }}
                                        />
                                    </View>
                                    <ProgressBarAndroid style={{ marginLeft: 5, width: screenSize.width - 285 }}
                                        styleAttr="Horizontal"
                                        indeterminate={false}
                                        color="#0276f1"
                                        progress={this.state.Progress_Value}
                                    />
                                    {(this.state.show_loader == true) ?
                                        (<View style={{ marginTop: 5, marginLeft: 5 }}>
                                            <LinesLoader size={30} barWidth={2} barHeight={30} barNumber={5} betweenSpace={3} color='#0276f1' />
                                        </View>)
                                        :
                                        null}
                                </View>)
                                :
                                null}
                        </View>)
                        :
                        null}


                    {(this.state.Projectimages.length > 0) ?

                        (
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <View style={{ marginTop: 20, flexDirection: "row" }}>
                                    {
                                        this.state.Projectimages.map((item) => (
                                            <View style={{ width: 120, height: 120 }}>
                                                <Image
                                                    source={{ uri: item }}
                                                    style={{ width: 100, height: 100 }}
                                                />
                                                <TouchableOpacity style={styles.deleteButton}
                                                    onPress={() => this.removeImageConfirmation(item)}
                                                >
                                                    <MaterialCommunityIcons name='close-circle' color='black' size={30} />
                                                </TouchableOpacity>
                                            </View>
                                        ))
                                    }


                                </View>
                            </ScrollView>)
                        : null}



                    <Button mode="contained" onPress={() => this.submitProject()}>
                        Submit Project Changes
            </Button>
                    <Button mode="contained" color={'#AF002A'} style={{ marginTop: 30 }} onPress={() => this.deleteProject()}>
                        Delete Project
            </Button>


                </KeyboardAwareScrollView>
                {(this.state.isLoading == true) ?
                    (<View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View>)
                    : null}
            </SafeAreaView>

        );
    }
}

const styles = StyleSheet.create({
    contentContainer: {
        padding: 16,
        width: screenSize.width
    },
    prompt: {
        marginBottom: 16,
        width: screenSize.width - 50,
    },
    input: {
        marginBottom: 16
    },
    helperText: {
        marginTop: -16,
        marginBottom: 16,
        marginLeft: 40
    },
    ImgaeUploadView: {
        marginTop: 10,
        padding: 16,
        width: screenSize.width - 32,
        backgroundColor: "#ffff",
        flexDirection: "row",
        elevation: 4,
        justifyContent: "space-between",
        alignItems: "center"
    },
    clickToUpload: {
        elevation: 1,
        borderColor: "#d2d1d1",
        justifyContent: "center",
        alignItems: "center",
        padding: 5
    },
    deleteButton: {
        position: "absolute",
        width: 60,
        height: 60,
        borderRadius: 30,
        right: 0,
        top: 0,
        alignItems: 'flex-end'
    },

    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});

