import React, { Component } from "react";
import { View, StyleSheet, ScrollView, Image, ActivityIndicator, Dimensions } from "react-native";
import { Button, Subheading, Title, List } from "react-native-paper";
import firebase from "react-native-firebase";
import { connect } from "react-redux";
import FastImage from "react-native-fast-image";

import LoginPlaceholder from "../components/login-placeholder";
import NavigationHeader from "../components/navigation-header";
import theme from "../theme";
import IconTextInput from "../components/icon-text-input";
import AsyncHeaderAction from "../components/async-header-action";
import DeviceInfo from 'react-native-device-info';

const screenSize = Dimensions.get('window');
const db = firebase.firestore();




const Header = ({ navigation }) => (
  <NavigationHeader
    style={styles.header}
    navigation={navigation}
    title="Manage Account Details"
  />
);

class AccountDetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: <Header navigation={navigation} />
  });

  state = {
    name: "",
    loading: false,
    isLoading: false,
    fcm_token:''
  };

  componentDidMount() {
    // AsyncStorage.getItem('fcm_token').then(value => this.setState({fcm_token: value }));
    firebase.analytics().setCurrentScreen("AccountDetails", "AccountDetails");
    this.setStateFromUser();
    //this._addFcmToken();
    this._addUser();
    
  }


  _addUser() {
    this.setState({ isLoading: true });
    const { user } = this.props;
    db.collection("users").doc(user.uid).set({
      firstname: user.displayName,
      lastname: "",
      email: user.email,
      phone: user.phoneNumber,
      imageLink: user.photoURL,
      //password:this.state.password,
      uid: user.uid,
      createdAt: user.creationTime,
      phVerify_status: 'N'
    }).then((user) => {
      this.setState({ isLoading: false });
    }).catch((error) => {
      this.setState({ isLoading: false });
      console.log("user error" + error)
    })
  }

 

  setStateFromUser = () => {
    this.setState({
      name: this.props.user.displayName
    });
  };

  updateField = fieldName => text => {
    this.setState({
      [fieldName]: text
    });
  };

  updateUser = async () => {
    const { name } = this.state;
    const { user } = this.props;
    console.log("user" + user);
    this.setState({ loading: true });
    try {
      await user.updateProfile({
        displayName: name.trim()
      });
      this._updateUserDetails();
    } catch (err) {
      console.log("Unable to update the user profile:", err);
      this.setState({ loading: false });
    }

  };

  cancel = () => {
    this.setStateFromUser();
  };
  _updateUserDetails() {
    const { user } = this.props;
    console.log("user" + JSON.stringify(user));
    db.collection("users").doc(user.uid).update({
      firstname: user.displayName
    }).then((user) => {
      this.setState({ loading: false });
    }).catch((error) => {
      this.setState({ loading: false });
      console.log("user error" + error);
    })

  }


  render() {
    const { user } = this.props;
    const { name, loading } = this.state;

    const hasUpdates = name && name !== user.displayName;

    if (!user) {
      return <LoginPlaceholder />;
    }

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <View>
          <IconTextInput
            icon="person"
            value={name}
            onChangeText={this.updateField("name")}
            label="Name"
            autoCapitalize="words"
            disabled={loading}
          />
        </View>

        <View style={styles.row}>
          <Button
            color={hasUpdates ? theme.colors.primary : theme.colors.disabled}
            style={styles.cancelButton}
            onPress={this.cancel}
            disabled={!hasUpdates}
          >
            Cancel
          </Button>
          <Button
            mode="contained"
            color={hasUpdates ? theme.colors.primary : theme.colors.disabled}
            diabled={!hasUpdates}
            loading={loading}
            onPress={this.updateUser}
          >
            Done
          </Button>
        </View>

        {(this.state.isLoading == true) ?
          (<View style={styles.activity_sub}>
            <ActivityIndicator
              size="large"
              color='#D0D3D4'
              style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
            />
          </View>)
          : null}
      </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  header: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0, 0, 0, .12)"
  },
  container: {
    backgroundColor: theme.colors.surface
  },
  contentContainer: {
    flex: 1,
    padding: 16,
    justifyContent: "space-between"
  },
  row: {
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  cancelButton: {
    color: theme.colors.disabled
  }
});

AccountDetailsScreen.propTypes = {};

const mapStateToProps = ({ auth }) => ({
  user: auth.user
});
export default connect(mapStateToProps)(AccountDetailsScreen);
