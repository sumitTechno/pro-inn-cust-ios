import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Platform,
  Linking,
  ActivityIndicator
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { Button, Subheading, Title } from "react-native-paper";
import { connect } from "react-redux";

import theme from "../theme";
import firebase from "react-native-firebase";
import { SafeAreaView } from "react-navigation";
import { Appbar } from "react-native-paper";

const Header = ({ navigation }) => (
  <SafeAreaView style={styles.header}>
    <Appbar.BackAction
      color={theme.colors.primary}
      onPress={() => navigation.goBack(null)}
    />
  </SafeAreaView>
);

const Strong = ({ children }) => (
  <Subheading style={styles.bold}>{children}</Subheading>
);

class SignInLoadingScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: <Header navigation={navigation} />
  });

  async componentDidMount() {
    //alert("check");
    firebase.analytics().setCurrentScreen("SignInLoading", "SignInLoading");
    await this.checkUser();
  }

  async componentDidUpdate() {
    await this.checkUser();
  }


  checkUser = async () =>  {
    //alert(this.props.user);
    console.log("Checking user", this.props.user);
    if (this.props.user) {
      this.props.navigation.popToTop()
    }
  };

  render() {
    const { email, authLoading } = this.props;
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <Title style={styles.condensed}>Almost Done!</Title>
        <View style={styles.iconContainer}>
          <MaterialIcons name="email" size={48} color={theme.colors.primary} />
        </View>
        <Subheading style={styles.text}>
          We sent an email to <Strong>{email}</Strong>. Please open this email{" "}
          <Strong>on this device</Strong> and follow the instructions to sign
          in.
        </Subheading>
        {!authLoading &&
          Platform.OS === "ios" && (
            <Button mode="outlined" onPress={this.openEmailClient}>
              Open Mail
            </Button>
          )}
        {authLoading && (
          <ActivityIndicator
            size="large"
            color={theme.colors.primary}
            animating={true}
          />
        )}
      </ScrollView>
    );
  }

  openEmailClient = async () => {
    if (Platform.OS === `ios`) {
      const inbox = await Linking.canOpenURL(`inbox-gmail:`);

      // if inbox is installed, open that
      if (inbox) {
        Linking.openURL(`inbox-gmail:`);
      } else {
        // else default to iOS Mail app
        Linking.openURL(`message:`);
      }
    }
  };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.surface,
    flex: 1
  },
  contentContainer: {
    alignItems: "center",
    padding: 16,
    flex: 1
  },
  iconContainer: {
    width: 64,
    height: 64,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 16
  },
  condensed: {
    marginBottom: 16,
    fontWeight:'900',
    fontSize:18
  },
  bold: {
    fontWeight: "700"
  },
  text: {
    marginBottom: 16
  },
  header: {
    backgroundColor: theme.colors.background
  }
});

SignInLoadingScreen.propTypes = {};

const mapStateToProps = ({ auth }) => ({
  email: auth.email,
  user: auth.user,
  authLoading: auth.loading
});

export default connect(mapStateToProps)(SignInLoadingScreen);
