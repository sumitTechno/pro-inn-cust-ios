import React, { Component } from "react";
import { SafeAreaView, 
    ScrollView, 
    Linking,
    View, 
    Text, 
    Dimensions,
    FlatList, 
    Platform,
    ActivityIndicator,
    RefreshControl,
    TouchableOpacity,
    Image
 } from "react-native";
import NavigationHeader from "../components/navigation-header";
import theme from "../theme";
import { Title, Subheading, Card, Appbar } from "react-native-paper";
import firebase from "react-native-firebase";
import handMessaging from "../../assets/images/hand-messaging.png";
import globalStyles from "../global-styles";
import LoginPlaceholder from "../components/login-placeholder";
import { connect } from "react-redux";
import { CachedImage } from 'react-native-cached-image';
import StarRating from 'react-native-star-rating';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const db = firebase.firestore();
const screenSize = Dimensions.get('window');

const Header = ({ navigation }) => (
    <SafeAreaView style={styles.header}>
        <Appbar.BackAction
            color={theme.colors.primary}
            onPress={() => navigation.goBack(null)}
        />
    </SafeAreaView>
);

class AccountDetailsReviewScreen extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <Header navigation={navigation} />

    });

    state = {
        uid : '',
        item : this.props.navigation.state.params.item,
        isLoading : false,
        profileImage : '',
        profileName : '',
        dataLength: 0,
        data1: [],
        data: [],

        isLoading: false,
        averageRating: 0,
    };

    componentDidMount() {
        if(this.props.user) {
            if (this.props.user.uid) {
                this.setState({
                  uid: this.props.user.uid
                });
              }
        }
        firebase.analytics().setCurrentScreen("AccountDetailsReview", "AccountDetailsReview");
        this._getProfileDetails(this.props.navigation.state.params.item.bidderId)
    }

    _stopLoading() {
        this.setState({ isLoading: false });
    }

    

    _showDate(date) {
        var dateValue =new Date(date)
        var dateReadable = dateValue.toDateString().trim();
        return (
               <Text >{dateReadable}</Text>
            
        )
    }

    _getProfileDetails(id)
    {
        this.setState({ isLoading: true })
        
        db.collection("users")
        .doc(id)
        .get()
        .then((doc) => {
            console.log('User data : ')
            console.log(doc.data());
            var fname = doc.data().firstname;
            var lname = doc.data().lastname;
            var name
            if(fname=='')
            {
                name='Pro';
            }
            else 
            {
                if(lname=='')
                {
                    name=fname;
                }
                else{
                    name=fname+' '+lname;
                }
            }
            this.setState({ profileImage : doc.data().imageLink, profileName : name, });
            this._getReviews(id)
        })
        .catch((err) => {
         this.setState({ isLoading: false });
        console.log(err)
        })
    }

    
    _getReviews(pro)
    {
        var totalRating=0;
        db.collection("customerReviews")
        .where("pro", "==", pro)
        .get().then((data) => {
            var array = [];

            data.docs.forEach(doc => {
                totalRating = totalRating+ doc.data().rating;
                console.log(totalRating)
                var obj=doc.data();
                array.push(obj);}
            )
            console.log('array length : '+ array.length)
            if(array.length==0)
            {
                this._stopLoading()
            }
            else{
                this.setState({
                averageRating : totalRating/array.length,
                dataLength: array.length,
                data1: array,
            }, this._getReviewerName.bind(this))
            }
            

        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log(err)
        })
    }


    _getReviewerName() {
        console.log('_getOwnerName');
        var dbData = [];
        var pData = this.state.data1;
         
        pData.forEach(data => {
              var name
            db.collection("users").doc(data.reviewer).get()
              .then(doc => {
                if (doc.exists) {
                    var projectData=data// project data

                    projectData.firstname=doc.data().firstname;
                    projectData.lastname=doc.data().lastname;
                    projectData.imageLink=doc.data().imageLink;
                    
                    if(projectData.imageLink)
                    {
                        if(projectData.imageLink=='')
                        {
                            projectData.imageLink=null;
                        }
                    }
                    else{
                        projectData.imageLink=null;
                    }
                    if(projectData.firstname==''|| projectData.firstname == undefined)
                    {

                        projectData.userName='Customer';
                    }
                    else
                    {
                        if(projectData.lastname)
                        {
                            if(projectData.lastname=='')
                            {
                                projectData.lastname=null;
                            }
                        }
                        else{
                            projectData.lastname=null;
                        }

                        //nameformating
                        var nameflag=projectData.firstname.indexOf(' ');
                        var nameflag1=projectData.firstname.indexOf('  ');
                        if(nameflag!=-1)
                        {
                            var nameArray = projectData.firstname.split(' ')
                            var name=nameArray[0]+' '+nameArray[1].charAt(0).toUpperCase();
                            projectData.userName=name;
                        }
                        else if(nameflag1!=-1) {
                            var nameArray = projectData.firstname.split('  ')
                            var name=nameArray[0]+' '+nameArray[1].charAt(0).toUpperCase();
                            projectData.userName=name;
                        }
                        else{
                            
                            if(projectData.lastname!=null)
                            {
                                var name=projectData.firstname+' '+projectData.lastname.charAt(0).toUpperCase();
                                projectData.userName=name;
                            }
                            else {
                                projectData.userName=projectData.firstname;
                            }
                        }
                    }
                  dbData.push(projectData);
                  //console.log('hi count '+dbData.length);
                  if(dbData.length==this.state.dataLength)
                    {
                        dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));       
                        
                        this.setState({
                            data: dbData,
                            
                        },this._stopLoading.bind(this));
                    }
                }
                 else {
                    var projectData=data
                    name = 'Customer';
                    //console.log(name);
                    projectData.userName=name;
                    projectData.imageLink=null;
                    dbData.push(projectData);
                    
                    if(dbData.length==this.state.dataLength)
                    {
                        dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));
                        this.setState({
                            data: dbData,
                            
                        },this._stopLoading.bind(this));
                    }
                }
              })
              .catch(err => {
                console.log(err);
                var projectData=data
                name = 'Customer';
                //console.log(name);
                projectData.userName=name;
                projectData.imageLink=null;

                dbData.push(projectData);
                if(dbData.length==this.state.dataLength)
                {
                    dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));
                    this.setState({
                        data: dbData,
                        
                    },this._stopLoading.bind(this));
                }
              });
          })
      }



    render() {
        const { user } = this.props;
        if (!user) {
            return <LoginPlaceholder />;
        }

        return (
            <View style={[globalStyles.surfaceContainer, { zIndex: 10 }]}>
                <View style={{ alignSelf: "center", marginBottom: 10, marginTop: -40, zIndex: 10 }}>
                    <Title>Account</Title>
                </View> 
                
                
                <View style={styles.ProfileView}>
                    <Image
                        source={this.state.profileImage?{
                                uri:
                                this.state.profileImage
                            }:
                        require('../../assets/images/user.png')}
                        style={styles.avatorImage}
                    />
                    <Text style={styles.profileName}>{this.state.profileName}</Text>
                    <View style={{flexDirection : 'row',alignItems : 'center'}} >
                    <StarRating
                        disabled={true}
                        emptyStar={'ios-star-outline'}
                        fullStar={'ios-star'}
                        halfStar={'ios-star-half'}
                        iconSet={'Ionicons'}
                        maxStars={5}
                        rating={this.state.averageRating}
                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                        fullStarColor={'#03a9f4'}
                        emptyStarColor={'#03a9f4'}
                        starSize={35}
                        starStyle={{ paddingLeft :2 }}
                    />
                    <Text style={{fontSize : 20,color : '#8e8e93'}}> {this.state.dataLength} Reviews</Text>    
                    </View>


                    </View> 

                {(this.state.dataLength == 0)?
                null
                :
                <ScrollView  showsVerticalScrollIndicator ={false}>
                <FlatList
                    data={this.state.data}
                    
                    renderItem={({ item }) => (
                    <View style={{width:screenSize.width, paddingLeft : 10,paddingRight : 10, backgroundColor : 'white',marginBottom : 2,borderBottomWidth : 0.3, borderBottomColor: '#999', paddingVertical : 2}}>
                        <View style={styles.imageHeadingContainer}>
                            <View style={{ flexDirection: "row" }}>
                            <Image
                            source={
                                item.imageLink?{
                                uri:
                                item.imageLink
                            }:
                            require('../../assets/images/user.png')}
                            style={styles.avatorImageSmall}
                            />
                            <View style={{ marginLeft: 10 ,flexDirection : 'row',alignItems : 'center',width:screenSize.width-90}} >
                                <View>
                                    <Text style={styles.ReviewerName}>{item.userName}</Text>
                                    <StarRating
                                        disabled={true}
                                        emptyStar={'ios-star-outline'}
                                        fullStar={'ios-star'}
                                        halfStar={'ios-star-half'}
                                        iconSet={'Ionicons'}
                                        maxStars={5}
                                        rating={item.rating}
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                        fullStarColor={'#03a9f4'}
                                        emptyStarColor={'#03a9f4'}
                                        starSize={20}
                                        starStyle={{ paddingLeft :2 }}
                                        containerStyle={{ width : 100 ,paddingTop : 2}}
                                    />
                                </View>
                                <Text style={styles.timeText}>{this._showDate(item.timeStamp)}</Text>
                            </View>
                            </View>
                            
                        </View>
                        <Text style={styles.reviewText}>
                        {item.review}
        
                        </Text>
                    </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
                </ScrollView> 

                }
                {(this.state.isLoading == true) ?
                (
                    <View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>   
                    </View>)
                : null}
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: theme.colors.background
    },
    ProfileView: {
        // position : 'absolute',
        // top : 0,
        // left : 0,
        width :  screenSize.width,
        height : 300,
        backgroundColor : 'white',
        marginBottom: 2,
        alignItems : "center",
        justifyContent : 'center',
        borderBottomWidth : 0.3,
        borderBottomColor: '#999',
        marginTop : -5,
        borderTopWidth : 0.5,
        borderTopColor : '#999',
    },
    header: {
        backgroundColor: theme.colors.surface
    },
    ReviewerName : {
        fontSize : 18,
        fontWeight : 'bold',
        marginBottom : 3
    },  
    timeText: {
        fontSize: 16,
        color: '#8e8e93',
        position : 'absolute',
        top : 35,
        right : 15
    },
    profileName: {
        fontWeight : 'bold',
        color: '#1e1e1e',
        fontSize: 25,
        marginTop: 10
    },
    reviewText: {
        color: '#555',
        fontSize: 17
        
    },
    contentContainer: {
        padding: 16,
        alignItems: "center"
    },
    email: {
        color: theme.colors.primary
    },
    title: {
        alignSelf: "center"
    },
    avatorImage: {
        width: 180,
        height: 180,
        borderRadius: 90,
        borderWidth: 2,
        borderColor: "#D0D3D4",
        marginTop : 10
    },
    avatorImageSmall: {
        width: 70,
        height: 70,
        borderRadius: 35,
        borderWidth: 2,
        borderColor: "#D0D3D4",
        marginTop : 10
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2.5,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },

};

AccountDetailsReviewScreen.propTypes = {};
const mapStateToProps = ({ auth }) => ({
    user: auth.user
});
export default connect(mapStateToProps)(AccountDetailsReviewScreen);