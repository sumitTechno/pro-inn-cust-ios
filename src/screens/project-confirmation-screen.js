import React, { Component } from "react";
import { ScrollView, StyleSheet, View, Image, TouchableOpacity, Text, ActivityIndicator } from "react-native";
import { Button, Subheading, Title } from "react-native-paper";
import globalStyles from "../global-styles";
import handyMan from "../../assets/images/handy-man.png";


class ProjectConfirmationScreen extends Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };

  state = {
    isLoading: true
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isLoading: false
      })
    }, 30000);

    // firebase
    //   .analytics()
    //   .setCurrentScreen("ProjectConfirmation", "ProjectConfirmation");
  }

  viewProjects = () => {
    //alert();
    this.props.navigation.popToTop();
    this.props.navigation.navigate("ProjectList");
  };

  close = () => {
    //alert();
    this.props.navigation.popToTop();
  };

  render() {
    return (
      <ScrollView
        style={globalStyles.surfaceContainer}
        contentContainerStyle={styles.contentContainer}
      >
        <Image style={styles.image} source={handyMan} />
        <View style={[styles.paddedContainer, { justifyContent: "center", alignItems: "center" }]}>
          <Title style={styles.condensed}>We'll take it from here.</Title>
          <Subheading style={styles.text}>
            A team member will be reaching out to you shortly!
          </Subheading>
          <View style={styles.buttonContainer}>
            <Button
              style={styles.closeButton}
              mode="outlined"
              onPress={this.close}
            >
              Close
            </Button>
            <Button mode="contained" onPress={this.viewProjects}>
              View projects
            </Button>
          </View>
        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    alignItems: "center",
    flex: 1
  },
  centered: {
    justifyContent: "center",
    alignItems: "center"
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  iconContainer: {
    width: 64,
    height: 64,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 16
  },
  paddedContainer: {
    padding: 16,

  },
  text: {
    paddingLeft: 16,
    paddingRight: 16,

  },
  buttonContainer: {
    marginTop: 16,
    flexDirection: "row"
  },
  closeButton: {
    marginRight: 16
  },
  image: {
    height: "50%",
    width: "100%",
    resizeMode: "contain",
    marginTop: 16
  }
});

ProjectConfirmationScreen.propTypes = {};

export default ProjectConfirmationScreen;
