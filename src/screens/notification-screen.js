import React, { Component } from "react";
import { SafeAreaView, 
    ScrollView, 
    Linking,
    View, 
    Text, 
    Dimensions,
    FlatList, 
    Platform,
    ActivityIndicator,
    RefreshControl,
    TouchableOpacity,
    Image
 } from "react-native";
import NavigationHeader from "../components/navigation-header";
import theme from "../theme";
import { Title, Subheading, Card, Appbar } from "react-native-paper";
import firebase from "react-native-firebase";
import handMessaging from "../../assets/images/hand-messaging.png";
import globalStyles from "../global-styles";
import LoginPlaceholder from "../components/login-placeholder";
import { connect } from "react-redux";
import { CachedImage } from 'react-native-cached-image';

const db = firebase.firestore();
const screenSize = Dimensions.get('window');

const Header = ({ navigation }) => (
    <SafeAreaView style={styles.header}>
        <Appbar.BackAction
            color={theme.colors.primary}
            onPress={() => navigation.goBack(null)}
        />
    </SafeAreaView>
);

class NotificationsScreen extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: <Header navigation={navigation} />

    });

    state = {
        notificationData : [],
        uid : '',
        data: [],
        isLoading : false,
        notificationDataLength : 0,
        Data : [],
        refreshing: false,
    };

    componentDidMount() {
        if(this.props.user) {
            if (this.props.user.uid) {
                this.setState({
                  uid: this.props.user.uid
                }, this._getNotifications.bind(this));
              }
        }
        firebase.analytics().setCurrentScreen("Notifications", "Notifications");
    }




    async _getNotifications() {
        this.setState({ isLoading: true });
        
          await db
            .collection("notifications").doc(this.state.uid).collection('notifications')
            .get()
            .then(data => {
              var dbData = [];
    
              data.forEach(doc => {
                var nData = doc.data();
                console.log("loop :  " + JSON.stringify(doc.data()));
    
                dbData.push(nData);
              });
              if(dbData.length!=0)
              {
                this.setState({
                notificationData: dbData,
                notificationDataLength: dbData.length
                },this._getOwnerName.bind(this));
              }
              else { 
                this.setState({ isLoading: false });
              }
                 
            })
            .catch(err => {
              this.setState({ isLoading: false });
              console.log('onget : '+err);
            });
        
      }




      async _getNotifications1() {
       // this.setState({ isLoading: true });
       this.setState({notificationDataLength : 0})
          await db
            .collection("notifications").doc(this.state.uid).collection('notifications')
            .get()
            .then(data => {
              var dbData = [];
    
              data.forEach(doc => {
                var nData = doc.data();
                console.log("loop :  " + JSON.stringify(doc.data()));
    
                dbData.push(nData);
              });
              if(dbData.length!=0)
              {
                this.setState({
                notificationData: dbData,
                notificationDataLength: dbData.length
                },this._getOwnerName.bind(this));
              }
              else { 
                this.setState({ isLoading: false });
              }
                 
            })
            .catch(err => {
              this.setState({ isLoading: false });
              console.log('onget : '+err);
            });
        
      }




      _stopLoading() {
        //console.log('details'+JSON.stringify(this.state.pendingDataMod))
      this.setState({ isLoading: false ,refreshing : false});
    }


    _onRefresh = () => {
        this.setState({refreshing: true});
        this._getNotifications1()
        }



      _getOwnerName() {
        var dbData = [];
        var Data = this.state.notificationData;
    
         Data.forEach(data => {
              var name
            db.collection("users").doc(data.sender).get()
              .then(doc => {
                console.log("_Owner Id :  " + data.sender);
                if (doc.exists) {
                    var userData=data
                    userData.firstname=doc.data().firstname;
                    userData.lastname=doc.data().lastname;
                    userData.imageLink=doc.data().imageLink;

                    if(userData.imageLink)
                    {
                        if(userData.imageLink=='')
                        {
                            userData.imageLink=null;
                        }
                    }
                    else{
                        userData.imageLink=null;
                    }
                    // if(doc.data().imageLink || doc.data().imageLink!='')
                    // userData.imageLink=doc.data().imageLink;
                    

                    //firstname
                    if(userData.firstname)
                    {
                        if(userData.firstname=='')
                        {
                            userData.ownerName='Customer';
                        }
                        else{
                            if(userData.lastname)
                            {
                                if(userData.lastname=='')
                                {
                                    userData.lastname=null;
                                }
                            }
                            else{
                                userData.lastname=null;
                            }

                            //nameformating
                            var nameflag=userData.firstname.indexOf(' ');
                            var nameflag1=userData.firstname.indexOf('  ');
                            if(nameflag!=-1)
                            {
                                var nameArray = userData.firstname.split(' ')
                                var name=nameArray[0]+' '+nameArray[1].charAt(0).toUpperCase();
                                userData.ownerName=name;
                            }
                            else if(nameflag1!=-1) {
                                var nameArray = userData.firstname.split('  ')
                                var name=nameArray[0]+' '+nameArray[1].charAt(0).toUpperCase();
                                userData.ownerName=name;
                            }
                            else{
                                
                                if(userData.lastname!=null)
                                {
                                    var name=userData.firstname+' '+userData.lastname.charAt(0).toUpperCase();
                                    userData.ownerName=name;
                                }
                                else {
                                    userData.ownerName=userData.firstname;
                                }
                            }
                        }
                    }
                    else{
                        userData.ownerName='Customer';
                    }
                    



                  dbData.push(userData);
                  //console.log('hi count '+dbData.length);
                  if(dbData.length==this.state.notificationDataLength)
                    {
                        dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));

                        this.setState({
                            Data: dbData,
                            
                        },this._stopLoading.bind(this));
                        //console.log('hi count ');
                    }
                }
                 else {
                    var userData=data
                    name = 'Customer';
                    console.log(name);
                    userData.ownerName=name;
                    userData.imageLink=null;
                    dbData.push(userData);
                   // console.log('hi count '+dbData.length);
                    if(dbData.length==this.state.notificationDataLength)
                    {
                        dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));
                        this.setState({
                            Data: dbData,
                            
                        },this._stopLoading.bind(this));
                       // console.log('hi count ');
                    }
                }
              })
              .catch(err => {
                console.log(err);
                var userData=data
                name = 'Customer';
                console.log(name);
                userData.ownerName=name;
                userData.imageLink=null;
                dbData.push(userData);
               // console.log('hi count '+dbData.length);
                if(dbData.length==this.state.notificationDataLength)
                {
                    dbData=dbData.sort((a,b) => (b.timeStamp > a.timeStamp) ? 1 : ((a.timeStamp > b.timeStamp) ? -1 : 0));
                    this.setState({
                        Data: dbData,
                        
                    },this._stopLoading.bind(this));
                    //console.log('hi count ');
                }
              });
          })
      }

      



    _showDate(time) {

        var date = new Date(time);
        var dateReadable = date.toDateString().trim();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        hours = hours < 10 ? '0'+hours : hours;
        var strTime = hours + ':' + minutes + ' ' + ampm;
       
        return (
        <View >
            <Text style={styles.timeText}>{dateReadable}  {strTime}</Text>
        </View>
        );
      }

_goToProjectDetails(projectId)
{
    this.setState({isLoading:true});
    var obj;
    db.collection("projects").doc(projectId).get()
    .then(doc => {
        if (doc.exists) {
            obj={
                id:projectId,
                ...doc.data()
            }
            this.setState({isLoading:false});
            this.props.navigation.navigate("ProjectDetails",{project:obj});
            console.log("object"+obj);    
        }
        
    })
    .catch(err=>{

    })
}

    render() {
        const { user } = this.props;
        if (!user) {
            return <LoginPlaceholder />;
        }

        return (
            <View style={[globalStyles.surfaceContainer, { zIndex: 10 }]}>
                <View style={{ alignSelf: "center", marginBottom: 10, marginTop: -40, zIndex: 10 }}>
                    <Title>Notifications</Title>
                </View>
                <View style={styles.container}>
                <ScrollView style={{width: screenSize.width ,position: "absolute",  top: 0, bottom  : 0}}refreshControl={
                    <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    />
                    }>
                <FlatList
                    data={this.state.Data}
                    
                    renderItem={({ item }) => (
                        <View style={{ backgroundColor: "#ffff" ,marginTop : 5}}>
                                   


                                        <TouchableOpacity activeOpacity={0.7}
                                            style={{ flexDirection: "row", padding: 16, width: screenSize.width, height: 100, borderBottomColor: "#d2d1d1", borderBottomWidth: 0.5 }}
                                            onPress={()=>this._goToProjectDetails(item.projectId)}
                                        >
                                            {/* <Image
                                                source={require('../../assets/images/userOwner.png')}
                                                style={styles.avatorImage}
                                            /> */}
                                            <CachedImage
                                                source={item.imageLink?
                                                    {uri: item.imageLink }
                                                    :
                                                    require('../../assets/images/user.png')}
                                                style={styles.avatorImage}
                                            />
                                            <View style={{ marginLeft: 10,width: screenSize.width-100, height: 100,position : 'absolute',top : 0,right : 5, justifyContent: 'space-between',paddingVertical : 5 }}>
                                            <Text style={{ fontSize: 14 }}>{item.subject}</Text>
                                            <Text style={{ fontWeight: "500", color: "black", fontSize: 15 }} numberOfLines={2} ellipsizeMode='tail'>{item.ownerName}
                                            {(item.status=='bidded')?<Text style={{ fontWeight: "100",fontSize: 14 }}> bid <Text style={{ fontWeight: "500" , color: '#555'}}>${item.bidPrice}</Text> on </Text> 
                                            :
                                            (item.status=='updated')?<Text style={{ fontWeight: "100",fontSize: 14 }}> Updated the bid to <Text style={{ fontWeight: "500", color: '#555'}}>${item.bidPrice}</Text> on your project </Text>
                                            :
                                            (item.status=='completed')?<Text style={{ fontWeight: "100",fontSize: 14 }}> has completed the project </Text>
                                            :null}
                                            <Text style={{ fontWeight: "500" ,color: '#555'}}> {item.projectDescription}</Text> 
                                            </Text>

                                            {this._showDate(item.timeStamp)}

                                            </View>
                                    </TouchableOpacity>
                                </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
                </ScrollView>

                </View>

                {this.state.isLoading == true ? (
          <View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
          <View style={styles.activity_sub}>
            <ActivityIndicator
              size="large"
              color="#D0D3D4"
              style={{
                justifyContent: "center",
                alignItems: "center",
                height: 50
              }}
            />
          </View>  
          </View>
          
        ) : null}
               
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: theme.colors.background
    },
    header: {
        backgroundColor: theme.colors.surface
    },
    contentContainer: {
        padding: 16,
        alignItems: "center"
    },
    email: {
        color: theme.colors.primary
    },
    title: {
        alignSelf: "center"
    },
    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2.5,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },

};

NotificationsScreen.propTypes = {};
const mapStateToProps = ({ auth }) => ({
    user: auth.user
});
export default connect(mapStateToProps)(NotificationsScreen);