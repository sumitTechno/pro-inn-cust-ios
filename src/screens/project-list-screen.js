import React, { Component } from "react";
import {
  SafeAreaView,
  ScrollView,
  Linking,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  Dimensions,
  ImageBackground,
  StatusBar,
  Alert,
  FlatList,
  RefreshControl,
} from "react-native";

import { connect } from "react-redux";
import firebase from "react-native-firebase";
import { Button, Card, Subheading, Title } from "react-native-paper";

import projects from "../api/project-collection";
import NavigationHeader from "../components/navigation-header";
import ProjectList from "../components/project-list";
import globalSyles from "../global-styles";
import LoginPlaceholder from "../components/login-placeholder";
import beachMan from "../../assets/images/beach-man.png";
import theme from "../theme";
import { SegmentedControls } from 'react-native-radio-buttons';

const db = firebase.firestore();
const screenSize = Dimensions.get('window');
const Header = ({ navigation }) => (
  <NavigationHeader showBackButton={false} navigation={navigation} title="" />
);

const options = [
  "In Progress",
  "Completed",
  "Trash"
];


class ProjectListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      // header: <Header navigation={navigation} />
      header: null
    };
  };

  state = {
    projects: [],
    projects1: [],
    archiveProjects: [],
    InProgressProjects: [],
    selectedOption: 'In Progress',
    refreshing: false,
    isLoading: false,
    deletedProjects: []
  };

  componentDidMount() {
    firebase.analytics().setCurrentScreen("ProjectList", "ProjectList");
    if (this.props.user) {
      this.fetchQuery();
    }
  }



  componentDidUpdate(prevProps) {
    //alert();
    if (!prevProps.user && !!this.props.user) {
      this.fetchQuery();
    }
    if (!!prevProps.user && !this.props.user) {
      if (this.unsubscribe) {
        this.unsubscribe();
      }
    }
  }

  fetchQuery = () => {
    this.query = projects.listMyProjects();
    this.unsubscribe = this.query.onSnapshot(this.onQueryUpdate);
    //this._update();
    console.log("unsub", this.unsubscribe);
  };


  onQueryUpdate = querySnapshot => {
    var project = [];
    project = querySnapshot.docs.map(doc => ({
      id: doc.id,
      ...doc.data()
    }));

    this.setState({
      projects: project
    }, this._update.bind(this))
  };


  _update() {
    console.log("projects" + this.state.projects);
    var archive = [];
    var inprogress = [];
    var deleted = [];

    for (var i = 0; i < this.state.projects.length; i++) {

      if (this.state.projects[i].projectStatus === "Completed") {
        console.log("match");
        archive.push(this.state.projects[i])
      } else if (this.state.projects[i].projectStatus === "Deleted") {
        console.log("match");
        deleted.push(this.state.projects[i])
      }
      else {
        console.log("not match");
        inprogress.push(this.state.projects[i])
      }

    }


    this.setState({
      archiveProjects: archive,
      InProgressProjects: inprogress,
      deletedProjects: deleted,
      refreshing: false
    });

    console.log("archive" + this.state.archiveProjects.length);
    console.log("in progress" + this.state.InProgressProjects.length);

  }


  setSelectedOption(selectedOption) {
    this.setState({
      selectedOption: selectedOption
    })

  }

  navigateToProject = project =>
    this.props.navigation.navigate("ProjectDetails", { project: project });

  navigateToHome = () => this.props.navigation.navigate("Home");

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.fetchQuery();
  }
  onEdit = (item) => {
    this.props.navigation.navigate("EditProject", { projectId: item.id });
  }

  onDelete = (project) => {
    Alert.alert(
      'Delete Project?',
      'Are you sure to delete this project?',
      [
        { text: 'Delete', onPress: () => this.apiDeleteProject(project) },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        }
      ],
      { cancelable: false },
    );
  }
  onUndo = (project) => {
    const unDoneAt = new Date().toISOString();
    db.collection("projects").doc(project.id).set({
      serviceId: project.serviceId,
      postalCode: project.postalCode,
      description: project.description,
      Pimage: project.Pimage,
      projectStatus: project.prevStatus,
      createdAt: unDoneAt,
      owner: project.owner,
      prevStatus: 'Deleted',
      updatedAt: unDoneAt,
    }).then((user) => {
      this.setState({ selectedOption: 'In Progress' })
      var q = db.collection("place_bid").where("projectId", "==", project.id);
      q.get().then(function (querySnapshot) {
        querySnapshot.docs.forEach(function (doc) {
          doc.ref.delete();
        })
      }
      )
    }).catch((error) => {
      alert("Something went wrong!");
    })
  }
  apiDeleteProject = project => {
    const deletedAt = new Date().toISOString();
    db.collection("projects").doc(project.id).update({
      serviceId: project.serviceId,
      postalCode: project.postalCode,
      description: project.description,
      Pimage: project.Pimage,
      projectStatus: 'Deleted',
      createdAt: project.createdAt,
      owner: project.owner,
      prevStatus: project.projectStatus,
      updatedAt: deletedAt,
    }).then((user) => {
      this.setState({ selectedOption: 'Trash' })
    }).catch((error) => {
      alert("Something went wrong!");
    })
  }

  render() {
    const { user } = this.props;
    if (!this.state.projects.length) {
      return (

        <SafeAreaView style={styles.placeholderContainer}>
          <ScrollView
            style={styles.placeholderContainer}
            contentContainerStyle={styles.placeholderContentContainer}
          >
            <Card>
              <Card.Cover source={beachMan} />
              <Card.Content>
                <Title style={styles.title}>Get Started</Title>
                <Subheading>
                  Create a project from our list of hundreds of services. Then,
                  sit back and let us do the work.
                </Subheading>
                <Button
                  mode="outlined"
                  style={styles.browseButton}
                  onPress={this.navigateToHome}
                >
                  Browse Services
                </Button>
              </Card.Content>
            </Card>
          </ScrollView>
        </SafeAreaView>
      );
    }


    return (
      <View style={[globalSyles.surfaceContainer]}>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >

          <View style={[styles.descriptionView, { marginTop: 5, marginBottom: 10 }]}>
            <SegmentedControls
              tint={'#315cab'}
              selectedTint={'white'}
              backTint={'#fff'}
              options={options}
              allowFontScaling={false} // default: true
              onSelection={this.setSelectedOption.bind(this)}
              selectedOption={this.state.selectedOption}
              optionStyle={{ fontSize: 16 }}
              optionContainerStyle={{ flex: 1 }}
            />
          </View>

          {(this.state.selectedOption == "In Progress") ?
            (<ProjectList
              projects={this.state.InProgressProjects}
              onProjectSelect={this.navigateToProject}
              onEditProject={this.onEdit.bind(this)}
              onDeleteProject={this.onDelete.bind(this)}
            />) : null}
          {(this.state.selectedOption == "Completed") ?
            (<ProjectList
              projects={this.state.archiveProjects}
              onProjectSelect={this.navigateToProject}
              onEditProject={this.onEdit.bind(this)}
              onDeleteProject={this.onDelete.bind(this)}
            />) : null}
          {(this.state.selectedOption == "Trash") ?
            (<ProjectList
              projects={this.state.deletedProjects}
              onProjectSelect={this.navigateToProject}
              onEditProject={this.onEdit.bind(this)}
              onDeleteProject={this.onDelete.bind(this)}
              onUndoDeletedProject={this.onUndo.bind(this)}
            />) : null}

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  placeholderContainer: {
    flex: 1,
    backgroundColor: theme.colors.background
  },
  placeholderContentContainer: {
    padding: 16
  },
  title: {
    alignSelf: "center"
  },
  browseButton: {
    marginTop: 16,
    alignSelf: "center"
  },
  descriptionView: {
    backgroundColor: "#ffff",
    //elevation: 5,
    padding: 15,
    width: screenSize.width,
  },

  activity_sub: {
    position: 'absolute',
    top: screenSize.height / 2,
    //backgroundColor: '#ffff',
    //width: 50,
    alignSelf: 'center',
    justifyContent: "center",
    alignItems: 'center',
    zIndex: 10,
    //elevation:5,
    ...Platform.select({
      android: { elevation: 5, },
      ios: {
        shadowColor: '#999',
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
      },
    }),
    //height: 50,
    //borderRadius: 10
  },

});

ProjectListScreen.propTypes = {};

const mapStateToProps = ({ auth }) => ({
  user: auth.user
});

export default connect(mapStateToProps)(ProjectListScreen);
