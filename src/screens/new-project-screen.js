import React, { Component } from "react";
import {
  StyleSheet,
  Platform,
  View,
  Dimensions,
  TouchableOpacity,
  ProgressBarAndroid,
  ProgressViewIOS,
  Image,
  FlatList,
  ScrollView,
  Text
} from "react-native";
import { HelperText, Subheading, Title, Button } from "react-native-paper";
import { connect } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import NavigationHeader from "../components/navigation-header";
import globalStyles from "../global-styles";
import IconTextInput from "../components/icon-text-input";

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { LinesLoader } from 'react-native-indicator';
import {
  setDescription,
  setPostalCode,
  setServiceId,
  setProjectImage,
  resetProject,
  setProjectVerified
} from "../redux/reducers/new-project.reducer";
import HeaderAction from "../components/header-action";
import { getServiceById } from "../api/services";
import firebase from "react-native-firebase";
import { isValidPostalCode } from "../util/validation";
import ImagePicker from 'react-native-image-picker';
import ImageResizer from "react-native-image-resizer";
import projects from "../api/project-collection";

const db = firebase.firestore();
const screenSize = Dimensions.get('window');
const { width, height } = Dimensions.get('window');


const Header = ({ navigation, onComplete }) => (
  <NavigationHeader
    navigation={navigation}
    //title="New Project"
    actions={[
      <HeaderAction />
    ]}
  />
);

class NewProjectScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    //header: null
    const { params } = navigation.state || {};
    const { onComplete } = params || {};

    return {
      header: <Header navigation={navigation} onComplete={onComplete} />
    };
  };

  state = {
    serviceName: "",
    postalCodeError: false,
    descriptionError: null,
    Progress_Value: 0.2,
    show_upload: false,
    show_loader: false,
    Projectimages: [],
    avatarSource: ""
  };

  componentDidMount() {
    firebase.analytics().setCurrentScreen("NewProject", "NewProject");
    const { navigation } = this.props;
    navigation.setParams({
      onComplete: this.submitProject
    });

    if (this.postalCodeInput && Platform.OS === "ios") {
      this.postalCodeInput.focus();
    }

    const service = getServiceById(this.props.serviceId);

    this.setState({
      serviceName: service ? service.name : ""
    });
  }

  componentDidUpdate() {
    let keyboardScrollView = this.refs.KeyboardAwareScrollView;

    if (keyboardScrollView) keyboardScrollView.update();
  }

  _clickToUpload() {
    this.setState({
      show_upload: true
    })
    this.getImage();
  }

  getImage() {
    const options = {
      title: 'Select Picture',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        //alert((response.fileSize / 1048576).toFixed(3));
        var resizedUri = "";
        var filename = "";
        const source = { uri: response.uri };
        var rotation;
        if (response.originalRotation === 90) {
          rotation = 90
        } else if (response.originalRotation === 270) {
          rotation = -90
        }
        ImageResizer.createResizedImage(response.uri, 1024, 1024, "JPEG", 80, rotation).
          then((responseImage) => {
            console.log("image resizer" + JSON.stringify(responseImage));
            resizedUri = responseImage.uri;
            this.setState({
              show_upload: true,
              avatarSource: source,
              show_loader: true
            });

            this._imageUp(responseImage.uri, responseImage.name);
          }).catch((err) => {
            console.log("err" + err);
          });
      }
    });

  }

  _resizeImage(uri) {
    var resizedUri;
    console.log("uri" + uri);
    ImageResizer.createResizedImage(uri, 600, 600, "JPEG", 80).
      then((response) => {
        console.log("image resizer" + response);
        resizedUri = response.uri;
        return resizedUri;
      }).catch((err) => {
        console.log("err" + err);
      });
    return resizedUri;
  }

  _imageUp(uri, name) {
    this.setState({ show_loader: true })
    var id = this.state.id;
    firebase.storage().ref('user-image').child(name)
      .put(uri, { contentType: 'image/jpeg' }) //--> here just pass a uri
      .then((snapshot) => {
        console.log(JSON.stringify(snapshot.metadata));
        firebase.storage().ref('user-image').child(name).getDownloadURL()
          .then((url) => {
            //this.setState({ imageLink: { uri: url } });
            var test = url;
            console.log("download" + url);
            this.state.Projectimages.push(url);
            Pimage = this.state.Projectimages;
            this.props.setProjectImage(this.state.Projectimages);
            this.Start_Progress();

          })
          .catch((error) => {
            this.setState({ show_loader: false });
            console.log("err download" + error);
          });
      })
      .catch((error) => {
        //reject(error);
        this.setState({ show_loader: false });
        console.log("error" + error);
      });
  }


  Start_Progress = () => {
    this.value = setInterval(() => {
      this.setState({ Progress_Value: this.state.Progress_Value + .01 })
    }, 100);
    this.setState({
      show_upload: false,
      Progress_Value: 0.00
    })
    console.log("image array" + this.state.Projectimages);
    console.log("progress value" + this.state.Progress_Value);

  }


  _delImage(value) {
    var array = this.state.Projectimages;
    array.forEach(function (element, i, object) {
      console.log(element);
      if (element.trim() == value.trim()) {
        //alert("del");
        object.splice(i, 1);
      }
      else {
        alert("no");
      }
    });
    this.setState({
      Projectimages: array
    })
    Pimage = this.state.Projectimages;
    this.props.setProjectImage(this.state.Projectimages);
    console.log("image" + this.state.Projectimages);
  }

  submitProject = async () => {
    if (!isValidPostalCode(this.props.postalCode)) {
      this.setState({
        postalCodeError: true
      });
      return;
    }
    this.setState({
      postalCodeError: false
    });
    firebase
      .analytics()
      .logEvent("create_project_tapped", { serviceId: this.props.serviceId });
    const { navigation, loggedIn } = this.props;
    if (!loggedIn) {
      this.props.setProjectVerified('N');
      this.createProject(this.props.newProject);
    } else {
      this.props.setProjectVerified('Y')
      this.createProject(this.props.newProject);
    }

    console.log("Project-----", this.props.projectVerified, this.props.newProject)
  };

  createProject = async () => {
    const { navigation, loggedIn } = this.props;
    try {
      await projects.createProject(this.props.newProject);
      console.log("*******" + JSON.stringify(this.props.newProject));
      if (!loggedIn) {
        navigation.navigate("NewAccount",{comingFrom:'createProject'});
      } else {
        navigation.navigate("CreateProjectLoading");
      }
    } catch (err) {
      this.setState({
        error: "Uh oh! Something went wrong. Please try again later."
      });
    }
  };

  render() {
    const {
      setPostalCode,
      setDescription,
      setProjectImage,
      postalCode,
      description,
      Pimage,
    } = this.props;
    return (
      <View style={[globalStyles.surfaceContainer, { zIndex: 10 }]}>

        <View style={{ alignSelf: "center", marginBottom: 10, marginTop: 0, zIndex: 10 }}>
          <Title>New project</Title>
        </View>


        <KeyboardAwareScrollView
          extraScrollHeight={100}
          keyboardShouldPersistTaps={false}
          ref="KeyboardAwareScrollView"
          //style={globalStyles.surfaceContainer}
          contentContainerStyle={styles.contentContainer}
        >
          <View style={{ alignSelf: "center" }}>
            <Title>{this.state.serviceName}</Title>
          </View>

          <View style={{ marginTop: 10, alignSelf: "center" }}>
            <Subheading style={styles.prompt}>
              Please provide your postal code and a brief description of what you
              need done.
            </Subheading>
          </View>

          <IconTextInput
            icon="place"
            value={postalCode || ""}
            onChangeText={setPostalCode}
            label="Postal code"
            style={styles.input}
            inputRef={ref => (this.postalCodeInput = ref)}
            keyboardType="number-pad"
            returnKeyType='done'
          />
          {this.state.postalCodeError && (
            <HelperText
              type="error"
              style={styles.helperText}
              visible={this.state.postalCodeError}
            >
              Please enter a valid postal code (e.g. 95014)
            </HelperText>
          )}
          <IconTextInput
            icon="edit"
            value={description || ""}
            onChangeText={setDescription}
            label="A short description of your project"
            multiline={true}
            style={styles.input}
          />


          {(Pimage.length < 5) ?
            (<View style={[styles.ImgaeUploadView, { marginBottom: 10, }]}>

              <TouchableOpacity style={[styles.clickToUpload, { justifyContent: "center", alignItems: "center" }]}
                onPress={() => this._clickToUpload()}
              >

                <View style={{ width: 30, height: 30, marginBottom: 10, marginTop: 10 }}>
                  <Image
                    source={require('../../assets/images/up.png')}
                    style={{ width: 30, height: 30 }}
                  />
                </View>
                <Text style={{ color: "#5f5e5e", fontSize: 12 }}>Click To upload image</Text>
              </TouchableOpacity>

              {(this.state.show_upload == true) ?
                (<View style={{ flexDirection: "row" }}>

                  <View style={{ width: 50, height: 50 }}>
                    <Image
                      source={this.state.avatarSource}
                      style={{ width: 50, height: 50 }}
                    />
                  </View>
                  <ProgressViewIOS style={{ marginLeft: 5, width: screenSize.width - 285, marginTop: 15 }}
                    styleAttr="Horizontal"
                    indeterminate={false}
                    color="#0276f1"
                    progress={this.state.Progress_Value}
                  />
                  {(this.state.show_loader == true) ?
                    (<View style={{ marginTop: 5, marginLeft: 5 }}>
                      <LinesLoader size={30} barWidth={2} barHeight={30} barNumber={5} betweenSpace={3} color='#0276f1' />
                    </View>)
                    :
                    null}
                </View>)
                :
                null}
            </View>)
            :
            null}


          {(Pimage.length > 0) ?

            (
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <View style={{ marginTop: 20, flexDirection: "row" }}>
                  {
                    Pimage.map((item) => (
                      <View style={{ width: 120, height: 120 }}>
                        <Image
                          source={{ uri: item }}
                          style={{ width: 100, height: 100 }}
                        />
                        <TouchableOpacity style={styles.deleteButton}
                          onPress={() => this._delImage(item)}
                        >
                          <MaterialCommunityIcons name='close-circle' color='black' size={30} />
                        </TouchableOpacity>
                      </View>
                    ))
                  }


                </View>
              </ScrollView>)
            : null}
          <Button mode="contained" onPress={() => this.submitProject()}>
            Submit Project
            </Button>


        </KeyboardAwareScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    padding: 16,
    width: screenSize.width
  },
  prompt: {
    marginBottom: 16,
    width: screenSize.width - 50,
  },
  input: {
    marginBottom: 16
  },
  helperText: {
    marginTop: -16,
    marginBottom: 16,
    marginLeft: 40
  },
  ImgaeUploadView: {
    marginTop: 10,
    padding: 16,
    width: screenSize.width - 32,
    backgroundColor: "#ffff",
    flexDirection: "row",

    ...Platform.select({
      android: {
        elevation: 4,

      },
      ios: {
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 1
        },
        shadowRadius: 2,
        shadowOpacity: 1.0,
      },
    }),
    justifyContent: "space-between",
    alignItems: "center"
  },
  clickToUpload: {
    elevation: 1,
    // ...Platform.select({
    //   android: {
    //     elevation:1,

    //   },
    //   ios: {
    //     shadowColor: '#000000',
    //     // shadowOffset: {
    //     //   width: 0,
    //     //   height:1
    //     // },
    //     shadowRadius:1,
    //     shadowOpacity: 1.0,
    //   },
    // }),

    borderColor: "#d2d1d1",
    borderWidth: 2,
    justifyContent: "center",
    alignItems: "center",
    padding: 5
  },
  deleteButton: {
    position: "absolute",
    width: 60,
    height: 60,
    borderRadius: 30,
    right: 0,
    top: 0,
    alignItems: 'flex-end'
  }
});

NewProjectScreen.propTypes = {};

const mapStateToProps = ({ newProject, auth }) => ({

  loggedIn: !!auth.user,
  description: newProject.description,
  postalCode: newProject.postalCode,
  serviceId: newProject.serviceId,
  Pimage: newProject.Pimage,
  projectVerified: newProject.projectVerified,
  newProject: newProject
});

export default connect(
  mapStateToProps,
  {
    setServiceId,
    setDescription,
    setPostalCode,
    setProjectImage,
    setProjectVerified,
    resetProject
  }
)(NewProjectScreen);
