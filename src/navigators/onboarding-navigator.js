import React from "react";
import { SafeAreaView, View, Image, Dimensions } from "react-native";
import { FluidNavigator, Transition } from "react-navigation-fluid-transitions";
import onboarding1 from "../../assets/images/onboarding-1.jpeg";
import onboarding2 from "../../assets/images/onboarding-2.jpeg";
import { Button, Title } from "react-native-paper";

const Screen1 = props => {
  const { width } = Dimensions.get("window");
  const imageSize = width * 0.8;
  return (
    <SafeAreaView style={styles.container}>
      <Transition appear="horizontal">
        <Image
          style={[styles.image, { width: imageSize, height: imageSize }]}
          source={onboarding1}
        />
      </Transition>
      <Transition appear="vertical">
        <Title style={styles.title}>
          Use Pro In Your Pocket to request projects for your home or business.
        </Title>
      </Transition>
      <Transition appear="horizontal">
        <View style={styles.buttonContainer}>
          <Button onPress={() => props.navigation.navigate("screen2")}>
            Next
          </Button>
        </View>
      </Transition>
    </SafeAreaView>
  );
};

const Screen2 = props => {
  const { width } = Dimensions.get("window");
  const imageSize = width * 0.8;
  return (
    <SafeAreaView style={styles.container}>
      <Transition appear="horizontal">
        <Image
          style={[styles.image, { width: imageSize, height: imageSize }]}
          source={onboarding2}
        />
      </Transition>
      <Transition appear="vertical">
        <Title style={styles.title}>
          Get quotes from local professionals that fit your budget!
        </Title>
      </Transition>
      <Transition appear="horizontal">
        <View style={styles.buttonContainer}>
          <Button onPress={() => props.navigation.goBack()}>Back</Button>
          <Button
            mode="contained"
            onPress={() => props.navigation.navigate("Tabs")}
          >
            Get started
          </Button>
        </View>
      </Transition>
    </SafeAreaView>
  );
};

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 16
  },
  image: {
    width: "80%",
    resizeMode: "contain"
  },
  title: {
    padding: 16,
    textAlign: 'center'
  },
  buttonContainer: {
    flexDirection: "row",
    marginBottom: 16
  }
};

const OnboardingNavigator = FluidNavigator({
  screen1: { screen: Screen1 },
  screen2: { screen: Screen2 }
});

export default OnboardingNavigator;
