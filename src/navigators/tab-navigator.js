import React from "react";

import HomeNavigator from "./home-navigator";
import createBottomTabNavigator from "../components/create-bottom-tab-navigator";
import ProjectListNavigator from "./project-list-navigator";
import AccountNavigator from "./account-navigator";
import MessagesNavigator from "./messages-navigator";
import NotificationsNavigator from "./notification-naviagator";

const TabNavigator = createBottomTabNavigator(
  {
    Home: HomeNavigator,
    ProjectList: ProjectListNavigator,
    Messaging: MessagesNavigator,
    Notification:NotificationsNavigator,
    Account: AccountNavigator
  },
  {
    initialRouteName: "Home",
    activeColor: "rgba(255, 255, 255, 1)",
    inactiveColor: "rgba(255, 255, 255, 0.4)"
  }
);

export default TabNavigator;
