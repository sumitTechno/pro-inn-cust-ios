import { createStackNavigator } from "react-navigation";
import tabBarIcon from "../components/tab-bar-icon";
import AccountScreen from "../screens/account-screen";
import AccountDetailsScreen from "../screens/account-details-screen";
import UpdateProfilePhotoScreen from "../screens/update-profile-photo-screen";

const AccountNavigator = createStackNavigator(
  {
    Account: AccountScreen,
    AccountDetails: AccountDetailsScreen,
    UpdateProfilePhoto: UpdateProfilePhotoScreen
  },
  {
    initialRouteName: "Account",
    headerMode: "screen"
  }
);

AccountNavigator.navigationOptions = {
  tabBarIcon: tabBarIcon("account-circle"),
  tabBarLabel: "Your Account",
};

export default AccountNavigator;
