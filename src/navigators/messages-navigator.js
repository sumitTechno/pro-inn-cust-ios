import { createStackNavigator } from "react-navigation";
import tabBarIcon from "../components/tab-bar-icon";
import MessagesScreen from "../screens/messages-screen";
import SendMessagesScreen from "../screens/send-message-screen";
import AccountDetailsReviewScreen from "../screens/account-details-review-screen";

const MessagesNavigator = createStackNavigator(
  {
    Messages: MessagesScreen,
    SendMessages:SendMessagesScreen,
    AccountDetailsReview:AccountDetailsReviewScreen
  },
  {
    initialRouteName: "Messages",
    headerMode: "screen"
  }
);

MessagesNavigator.navigationOptions = {
  tabBarIcon: tabBarIcon("chat-bubble"),
  tabBarLabel: "Messages",
};

export default MessagesNavigator;
