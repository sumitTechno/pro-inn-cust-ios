import { createStackNavigator } from "react-navigation";
import HomeScreen from "../screens/home-screen";
import NewProjectScreen from "../screens/new-project-screen";
import tabBarIcon from "../components/tab-bar-icon";
import React from "react";
import NewAccountScreen from "../screens/new-account-screen";
import AwaitingEmailScreen from "../screens/awaiting-email-screen";
import CreateProjectLoadingScreen from "../screens/create-project-loading-screen";
import SearchScreen from "../screens/search-screen";
import SearchNavigator from "./search-navigator";
import ProjectConfirmationScreen from "../screens/project-confirmation-screen";
import MessagesScreen from "../screens/messages-screen";
import SendMessagesScreen from "../screens/send-message-screen";
//import ProjectListScreen from "../screens/project-list-screen";

const HomeNavigator = createStackNavigator(
  {
    Home: {
      screen: SearchNavigator,
      navigationOptions: {
        header: null
      }
    },
   
    NewProject: NewProjectScreen,
    NewAccount: NewAccountScreen,
    AwaitingEmail: AwaitingEmailScreen,
    CreateProjectLoading: CreateProjectLoadingScreen,
    Search: SearchScreen,
    ProjectConfirmation: ProjectConfirmationScreen,
    Messages: MessagesScreen,
    SendMessages:SendMessagesScreen,
    //ProjectList: ProjectListScreen
  },
  {
    initialRouteName: "Home",
    headerMode: "screen"
  }
);

HomeNavigator.navigationOptions = {
  tabBarIcon: tabBarIcon("search"),
  tabBarLabel: "Discover",
};

export default HomeNavigator;
