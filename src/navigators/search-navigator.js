import { createFluidNavigator } from "react-navigation-fluid-transitions";
import HomeScreen from "../screens/home-screen";
import SearchScreen from "../screens/search-screen";

const SearchNavigator = createFluidNavigator({
  FeaturedServices: {
    screen: HomeScreen
  },
  Search: {
    screen: SearchScreen,
    navigationOptions: {
      gesturesEnabled: true
    }
  }
});

export default SearchNavigator
