import React from "react";
import { createStackNavigator } from "react-navigation";
import TabNavigator from "./tab-navigator";
import SignInNavigator from "./sign-in-navigator";
import OnboardingNavigator from "./onboarding-navigator";

const RootNavigator = createStackNavigator(
  {
    Tabs: TabNavigator,
    SignIn: SignInNavigator,
    Onboarding: OnboardingNavigator
  },
  {
    initialRouteName: "Tabs",
    mode: "modal",
    headerMode: "none"
  }
);

export default RootNavigator;
