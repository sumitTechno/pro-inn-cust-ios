import { createStackNavigator } from "react-navigation";
import SignInScreen from "../screens/sign-in-screen";
import SignInLoadingScreen from "../screens/sign-in-loading-screen";

const SignInNavigator = createStackNavigator(
  {
    SignIn: SignInScreen,
    SignInLoading: SignInLoadingScreen
  },
  {
    initialRouteName: "SignIn"
  }
);

export default SignInNavigator;
