import { createStackNavigator } from "react-navigation";
import tabBarIcon from "../components/tab-bar-icon";
import NotificationsScreen from "../screens/notification-screen";
import AccountDetailsReviewScreen from "../screens/account-details-review-screen";

const NotificationsNavigator = createStackNavigator(
  {
    Notifications:  NotificationsScreen,
    AccountDetailsReview:AccountDetailsReviewScreen,
    AccountDetailsReview:AccountDetailsReviewScreen
  },
  {
    initialRouteName: "Notifications",
    headerMode: "screen"
  }
);

NotificationsNavigator.navigationOptions = {
  tabBarIcon: tabBarIcon("notifications"),
  tabBarLabel: "Notifications",
};

export default NotificationsNavigator;
