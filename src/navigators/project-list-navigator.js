import { createStackNavigator } from "react-navigation";
import ProjectListScreen from "../screens/project-list-screen";
import tabBarIcon from "../components/tab-bar-icon";
import ProjectDetailsScreen from "../screens/project-details-screen";
import ReviewScreen from "../screens/review-screen";
import AccountDetailsReviewScreen from "../screens/account-details-review-screen";
import SendMessagesScreen from "../screens/send-message-screen";
import EditProjectScreen from '../screens/edit-project-screen'

const ProjectListNavigator = createStackNavigator(
  {
    ProjectList: ProjectListScreen,
    ProjectDetails: ProjectDetailsScreen,
    SendMessages: SendMessagesScreen,
    Review: ReviewScreen,
    AccountDetailsReview: AccountDetailsReviewScreen,
    EditProject: EditProjectScreen
  },
  {
    initialRouteName: "ProjectList",
    headerMode: "screen"
  }
);

ProjectListNavigator.navigationOptions = {
  tabBarIcon: tabBarIcon("list"),
  tabBarLabel: "Your Projects",
};

export default ProjectListNavigator;
