import ImageResizer from "react-native-image-resizer";

export async function resizeImageForUpload(uri,rotation) {
  const response = await ImageResizer.createResizedImage(
    //uri,
    uri, 
    800, 
    600, 
    "JPEG", 
    80, 
    rotation 
  );
  // response.uri is the URI of the new image that can now be displayed, uploaded...
  // response.path is the path of the new image
  // response.name is the name of the new image with the extension
  // response.size is the size of the new image
  return response.uri;
}

