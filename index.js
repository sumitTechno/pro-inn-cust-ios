/** @format */
import * as React from "react";
import { AppRegistry } from "react-native";
import { Provider as PaperProvider } from "react-native-paper";

import App from "./src/App";
import { name as appName } from "./app.json";
import theme from "./src/theme";
import { ReduxProvider } from "./src/redux";

export const Main = () => (
  <ReduxProvider>
    <PaperProvider theme={theme}>
      <App />
    </PaperProvider>
  </ReduxProvider>
);

AppRegistry.registerComponent(appName, () => Main);
